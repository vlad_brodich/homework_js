'use strict'// Суворий режим.
import { getElemId,rotateSlides, errorWindow,getDate } from "./functions.js";


// Початок роботи після завантаження сторінки.
window.addEventListener('load',()=>{

  // Функція згортання випадаючого меню. 
  window.addEventListener ('click',(event) => {
    if (event.target !== getElemId("btnShowMenu")) {
      getElemId("myDropdown").classList.remove("show");
    };
  });

  // Кнопка випадаючого меню. 
  getElemId("btnShowMenu").addEventListener ('click', () => {getElemId("myDropdown").classList.add("show")});

  // Виклик функції "слайдер"
  window.setInterval(function(){rotateSlides()}, 5000);
});

// Домашня робота.
// Задача 1 
// Вам потрібно написати програму, яка буде отримувати інформацію з сервера за допомогою fetch запиту, а потім виводити цю інформацію на сторінку. Для цього вам потрібно скласти запит до API за посиланням https://jsonplaceholder.typicode.com/users, який поверне список користувачів у форматі JSON.

// Після того, як ви отримаєте відповідь з сервера, вам потрібно перетворити JSON-об'єкт на масив об'єктів за допомогою методу json(), який є частиною об'єкта Response. Далі вам потрібно використати цей масив для створення HTML-елементів, які будуть містити інформацію про кожного користувача.

// Наприклад, ви можете створити таблицю з наступними стовпцями: ID користувача, ім'я, email і номер телефону. Для створення таблиці ви можете використати HTML-елементи <table>, <thead>, <tbody>, <tr> і <td>.

// Крім того, вам потрібно додати обробник помилок для вашого fetch запиту. Якщо запит не вдається, програма повинна вивести на сторінку повідомлення про помилку.

// Змінна лоудер.
const loader = getElemId('box-loader')

// Функція 'fetch' запитів на сервер.
const request = async (url) => {
  loader .classList.add("show-loader")
  const data = await fetch(url);
  const dataUser = await data.json()
  if(data.ok){
    show(dataUser)
  }
  else {
    loader.classList.remove("show-loader") 
    errorWindow (`Помилка виконання запититу.<br> Код помилки: ${data.status}`)
  }
}

// Функція виводу результатів на сторінку.
function show(data = []) {
  if (!Array.isArray(data)) return;
  const tbody = getElemId('work-2');
  tbody.innerHTML = ""
  data.forEach(({id, name, username,  email, phone, website }) => {
      tbody.insertAdjacentHTML("beforeend", `
      <tr> 
      <td>${id}</td>
      <td>${name}</td>
      <td>${username}</td>
      <td> <a href="mailto:${email}">${email}</a></td>
      <td>${phone}</td>
      <td>${website}</td>
      </tr>
      `)
  })
  loader.classList.remove("show-loader")
}
// Виклик функції запиту на сервер.
request('https://jsonplaceholder.typicode.com/users');



// Задача 2 
// Вам потрібно написати програму, яка буде працювати з подіями. Ваша програма повинна відслідковувати події кліків на сторінці та зберігати координати місця, де було здійснено клік. Після того, як буде здійснено 10 кліків, програма повинна показати користувачу список всіх координат, де були здійснені кліки.

// Для виконання цієї задачі вам потрібно використати обробники подій. Ви можете використати метод addEventListener() для відслідковування подій кліків миші на сторінці. Після кожного кліку на сторінці, вам потрібно зберігати координати місця, де було здійснено клік, у масив. Якщо кількість кліків досягне 10, вам потрібно вивести на сторінку список всіх координат з масиву.

// Успіхів у виконанні!


// Оголошення масиву для зберігання координат.
let newArr = [];

// Функція виводу результатів на сторінку.
function showElementClick (arr){
  if (!Array.isArray(arr)) return;
  const tbody = getElemId('work-4');
  tbody.innerHTML = ""
  arr.forEach(({id, elem, x,  y, time}) => {
      tbody.insertAdjacentHTML("beforeend", `
      <tr> 
      <td>${id}</td>
      <td>${elem}</td>
      <td>${x} px</td>
      <td>${y} px</td>
      <td>${time}</td>
      </tr>
      `)
  })
};

// Лічильник кліків.
let caunt = 1;
// Слухач події 'click' та запис координат події.
window.addEventListener('click',(ev)=>{
  const result = {id:caunt,time: getDate(),elem:ev.target.nodeName,x : ev.clientX, y: ev.clientY};
  if(caunt < 10){
    newArr.push(result)
    getElemId('work-3').innerHTML = `Кількість кліків на сторінці: ${caunt}`
    caunt +=1;
  }
  else {
    newArr.push(result)
    showElementClick(newArr)
    newArr = []
    getElemId('work-3').innerHTML = `Кількість кліків на сторінці: ${caunt}`
    caunt = 1
  }
});