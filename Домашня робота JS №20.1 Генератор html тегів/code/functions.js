'use strict'// Суворий режим.

// Функція пошуку елемента по id.
export const getElemId = id => document.getElementById(id);

//Функція "Новий-слайдер" на шість фото.
export function rotateSlides() {
  // Знаходимо слайд.
  const slider = getElemId('slider');
  // Знаходимо дочірній вузол слайду.
  const elemLastChild = slider.lastElementChild;
  // Клонування дочірнього вузла разом з вмістом.
  const lastChild = elemLastChild.cloneNode(true);
  // Всі слайди в колекції прибираємо клас 'firstSlide'.
  const [...sliders] = document.querySelectorAll('#slider li');
  sliders.forEach(el=>{el.classList.remove('firstSlide');});
  // Видаляємо дочірній елемент з попередньої позиції.
  elemLastChild.remove(elemLastChild);
  // Додаємо клон на нову позицію.
  slider.prepend(lastChild);
  // Додаємо клас 'firstSlide'.
  lastChild.classList.add('firstSlide')
}


// Функції домашньої роботи.

// Функція тест валідація.
export const validate = (p, v) => p.test(v);

// Функція створення тегів.

 export function creatHtmlElem (tagName = "div", className,text = '', tadId, value = "" ){
  const element = document.createElement(tagName);
  if(className){
    element.classList.add(className);
  }
  if(tadId){
    element.id = tadId;
  }
  element.innerText = text;
  element.value = value;
  return element
}

//Універсальна  функція створення HTML тегів за допомогою createElement.
export function createHtmlElements (tagName = "div", className, value = "", attr = [], listener, context) {
  const element = document.createElement(tagName);
  if(className){
      element.classList.add(className);
  }
  element.innerHTML = value;
  attr.forEach((obj)=>{
      const objectInfo = Object.entries(obj);
      element.setAttribute(objectInfo[0][0], objectInfo[0][1])
  })

  if("inputtextariaselect".includes(tagName)){
      if(!listener) return element
      element.addEventListener("change", (e) => {
          if(context){
             listener(context) 
          }else{
              listener(e)
          }
      })
  }else{
      element.addEventListener("click", (e) => {
          if(!listener) return element
          if(context){
             listener(context) 
          }else{
              listener(e)
          }
      })
  }
  return element
}

// Функція створює поточну дату та час.
export const getDate = () =>{
  const date = new Date();
  let day = ''
  let month = '';
  let second = '';
  if(date.getDate() > 9){
    day = date.getDate()
  }else{day = `0${date.getDate()}`}

  if(date.getMonth()+1 > 9){
    month = date.getMonth()+1;
  }else{month = `0${date.getMonth()+1}`}

  if(date.getSeconds() > 9 ){
    second = date.getSeconds()
  }else{second =`0${date.getSeconds()}`}

  const dataNew = [date.getFullYear(),month,day,`${date.getHours()}:${date.getMinutes()}`,second]
  return dataNew.join('.')
}

// Функція виводу вікна помилки.
export function errorWindow (text,time = '2000'){
  const el = document.createElement('div');
  el.className = "error-box";
  el.innerHTML = `<p>${text}</p>`
  document.body.prepend(el);
  setTimeout(()=>{
    el.remove(el)
  },time);
  // window.addEventListener ('click',() => {
  //   el.remove(el)
  // });
}
