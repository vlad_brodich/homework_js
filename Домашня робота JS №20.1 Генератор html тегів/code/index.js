'use strict'// Суворий режим.
import { getElemId, rotateSlides, creatHtmlElem, errorWindow, validate } from "./functions.js";


// Початок роботи після завантаження сторінки.
window.addEventListener('load',()=>{

  // Функція згортання випадаючого меню. 
  window.addEventListener ('click',(event) => {
    if (event.target !== getElemId("btnShowMenu")) {
      getElemId("myDropdown").classList.remove("show");
    };
  });

  // Кнопка випадаючого меню. 
  getElemId("btnShowMenu").addEventListener ('click', () => {getElemId("myDropdown").classList.add("show")});

  // Виклик функції "слайдер"
  window.setInterval(function(){rotateSlides()}, 5000);
});

// Домашня робота.
// Задача.
// Робимо роботу до 3х владеностей (дії). Тобто не більше ніж 
// .one>div^a - видасть 
//    <div class="one">
//         <div></div>
//     </div>
//     <a href=""></a>
// .one>div^a>span - вже помилка.

//     . - класс
//     # - id
//     > 1й рівень вкладеності
//     div - та інші назви тегів
//     {} - текст
// На навій строці нові інструкуці (Якщо в тексейрі перейшли на нову строку, то видаємо як нову вкладеність)




// Тимчасовий  масив об'єктіів.
let tags = [];
// Функція виводу результатів на сторінку.
function showWindowResult(tag1,tag2 = '',tag3 = ''){
  let elm1 = '';
  let elm2 = '';
  let elm3 = '';

  for(let i = 0; i < tag1.quantity*1; i += 1){
    elm1 = creatHtmlElem (tag1.tagName, tag1.className, tag1.tagText, tag1.tagId);
    for(let a=0; a < tag2.quantity*1; a += 1){
      if(tag2 !== ''){
        elm2 = creatHtmlElem (tag2.tagName, tag2.className, tag2.tagText, tag2.tagId);
        elm1.append(elm2);
      }
      for(let b = 0; b < tag3.quantity*1; b += 1){
        if(tag3 !== ''){ elm3 = creatHtmlElem (tag3.tagName, tag3.className, tag3.tagText, tag3.tagId);
          elm2.append(elm3); 
        }
      }
    }
   getElemId('window-result').append(elm1);
  }
}

// Функція визначає порядок виводу тегів та їх вкладеність.
function resultShow(arr){
  let a = '';
  let b = '';
  let c = '';
  let d = '';
  arr.forEach((el,i) => {
    if(el.numAttachment*1 === 0){
      if(i < 2){
        showWindowResult(el);
      }
      else d = el;
    } 
    else if(el.numAttachment*1 === 1){
      a = el;
    }
    else if(el.numAttachment*1 === 2){
      b = el;
    }
    else if(el.numAttachment*1 === 3){
      c = el;
    }
  });
  if(d === ''){
    showWindowResult(a,b,c);
  }
  else {
    showWindowResult(a,b,c);
    showWindowResult(d);
  }
}


// Функція очищення тимчасового об'єкту.
function cleaning(){
  tag.numAttachment = '',
  tag.tagName = '';
  tag.className= '';
  tag.tagId= '';
  tag.tagText='';
  tag.quantity = '';
}

// Шаблон тегів.
const patern = "a div abbr article aside audio button section main header footer form h1 h2 h3 h4 h5 h6 img input ul li nav ol p span table tbody td th thead tfoot tr textarea label select map address canvas caption datalist hr iframe";

// Клас для створення об'єктіів.
class Tags{
  constructor({numAttachment, tagName, className,  tagId, tagText, quantity}){
    if(numAttachment === '') {this.numAttachment = 0} else this.numAttachment = numAttachment;
    if(tagName === ''|| !patern.includes(tagName.toLowerCase()) || validate(/[0-9]/, tagName)) {
      errorWindow (`${tagName} - Неприпустима назва тегу. За замовчуванням буде створено 'DIV'`,4000);
      this.tagName = 'div';
    } else this.tagName = tagName.toLowerCase();
    this.className = className;
    this. tagId =  tagId;
    this.tagText = tagText;
    if(quantity === ''){this.quantity = 1}else this.quantity = quantity;
  }
}

// Тимчасовий об'єкт.
const tag = {
  numAttachment :'',
  tagName :'',
  className: '',
  tagId: '',
  tagText:'',
  quantity: '',
};
// Змінна імені тегу.
let im = 'tagName';
// Змінна лічильника вкладеностей.(обмеження в умові завдання.)
let numAttachments = 1;
// Індикатор вкладеностей для обєкта.
let numIdElem = 1;
// Змінна  інфо з інпуту.
let str = '';
// Змінна  останньої інфо з інпуту.
let strTag = '';
// Обробник строки.
function handlerResult (str){
  // Перевірка чи строка не порожня.
  if(str === ''){
    return
  };
// Тимчасовий масив.
  let arr = str.split(''); 
  im = 'tagName';
  arr.forEach((el,i) => {
    if(el === '.'){
      im = 'className'; 
      return 
    }
    else if(el === '#'){
      im = 'tagId';
      return
    }
    else if(el === '{'){
      im = 'tagText'; 
      return
    }
    else if(el === '}'){
      im = 'tagName';
      return
    }
    else if(el === '*'){
      im = 'quantity';
      return
    }
    else if(el === '>'){
    im = 'tagName';
      if(numIdElem < 3){
        tag.numAttachment = numIdElem;
        tags.push(new Tags(tag));
        cleaning()
        numAttachments += 1;
        numIdElem += 1;
        return;
      }
      else{
        arr.splice(i,arr.length - i);
        errorWindow ("Перевищена кількість вкладеностей 3, Зайве буде видалено!");
        return 
      }
    }
    else if(el === '^'){
      im = 'tagName';
      if(numAttachments < 3){
        if(tags.length === 0){
          tag.numAttachment = 0;
          tags.push(new Tags(tag));
          cleaning()
          numAttachments += 1;
          numIdElem = 1;
        }
        else if(tags.length === 1 && tags[0].numAttachment === 0){
          tag.numAttachment = 0;
          tags.push(new Tags(tag));
          cleaning()
          numAttachments += 1;
          numIdElem = 1;
        }
        else{
          tag.numAttachment = numIdElem;
          tags.push(new Tags(tag));
          cleaning()
          numAttachments += 1;
          numIdElem = 1;
        }
        return;
      }
      else{
        arr.splice(i,arr.length - i);
        errorWindow ("Перевищена кількість вкладеностей 3. Зайве буде видалено!");
        return 
      }
    }  
    else tag[im] += el;
  });
  if(tags.length === 2 && numIdElem === 1){
    im = 'tagName'
    tag.numAttachment = 0;
    tags.push(new Tags(tag));
    cleaning()
    numAttachments = 1;
    numIdElem = 1;
    resultShow (tags);
    console.log(tags)
    tags = [];
  }
  else{
    im = 'tagName'
    tag.numAttachment = numIdElem;
    tags.push(new Tags(tag));
    cleaning();
    numAttachments = 1;
    numIdElem = 1;
    resultShow (tags);
    console.log(tags)
    tags = [];
  }
};

// Слухач події клавіатури подія Enter.
getElemId('window-input').addEventListener('keydown',(ev) =>{
  if(ev.key === 'Enter'){
    str = ev.target.value.split(/\r\n|\r|\n/g);
    strTag = str[str.length-1].trim();
    handlerResult (strTag);
  }  
});

// Слухач події 'change'.
getElemId('window-input').addEventListener('change',(ev) =>{
  if(strTag === ""){
    return
  }
  else {
    str = ev.target.value.split(/\r\n|\r|\n/g);
    strTag = str[str.length-1].trim();
    handlerResult (strTag) 
    // ev.target.value = '';
  }
});

