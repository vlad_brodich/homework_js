'use strict'// Суворий режим

/*
1. Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test"
focus blur input */

// Чекаємо завантаження всієї сторінки
window.addEventListener('load',() =>{
    // Отримуємо список всіх інпутів
    const [...inputs] = document.getElementsByTagName('INPUT');
    // Перебираємо список
    inputs.forEach((input)=>{
        // Вішаємо слухач події 'blur' на ті інпути в яких є "dataset".
        if(input.dataset.length){
            input.addEventListener ('blur', () =>{volidateInput(input)}); 
        }
        // Вішаємо слухач події 'blur' на всі інпути.
        input.addEventListener ('blur', () => {
            get("test").insertAdjacentHTML("beforeend",`<p>Втратив фокус input value: ${input.value}</P>`);
        });
    });

    /*
    2.Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.*/

    // Функція перевірки значення dataset-length.
    const volidateInput = (el) => {
        if(Number(el.dataset.length) === el.value.length){
            el.className = '';
            el.classList.add('green-input');
        }
        else  {
            el.className = '';
            el.classList.add('red-input');
        }
    };
});
/* 3. - При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень.
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору.    При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 'Число: (введене значення)'.
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

// Чекаємо завантаження DOM-дерева.
window.addEventListener("DOMContentLoaded", () => {
    // Створюємо модальне вікно з полем введення `Price`
    get("main-page").insertAdjacentHTML("afterBegin",`<div class="form-use" id="form-use">
    <img src="./imege/Close.svg" alt="Close" width="30" id="close">
    <input class="input-use" type="number" id="numberUse" placeholder="Price">
    <span class="error" id="error">'Please enter correct price'</span>
    </div>`);
    // Створення змінних форми
    const errorUse = get('error'),
    inputUse = get('numberUse'),
    formUse = get('form-use');
    // Подія клік для кнопки закриття форми
    get('close').addEventListener ('click', () =>{  
        formUse.remove(formUse);
    });
    // Поведінка інпута на подію 'focus'.
    inputUse.addEventListener ('focus', () =>{
        inputUse.value = '';
        inputUse.className = 'input-use';
        errorUse.classList.remove('error-show');
        inputUse.classList.add('green-input');
    });
     // Поведінка інпута на подію "втрата фокусу".
    inputUse.addEventListener ('blur', () =>{
        inputUse.className = 'input-use';
        // Перевірка значення яке ввів користувач.
        // Як що значення менше за 0.
        if(inputUse.value < 0){
            if(get("result")){
                get("result").remove(get("result"));
            }
            inputUse.className = 'input-use';
            // Вивід тексту `Please enter correct price`.
            errorUse.classList.add('error-show');
            inputUse.classList.add('red-input');
        }
         // Як що значення більше за 0 та не порожня строка.
        else if(inputUse.value !== ''){
            inputUse.className = 'input-use';
            if(!get("result")){
                // Вивід `span` з повідомленням та `X`.
                formUse.insertAdjacentHTML("afterBegin",`
                <span id='result'>Число: ${inputUse.value}. <span class="closeX" id="closeX">×</span></span>`);
            }
            // Оновлення повідомлення.
            else {get("result").innerHTML = `Число: ${inputUse.value}. <span class="closeX" id="closeX">×</span>`;
            }
            // Подія клік для `X`.
            get("closeX").addEventListener ('click', () =>{
                get("result").remove(get("result"));
            });
            inputUse.classList.add('green-input-text');
        };  
    });
});