import { changeInputAuth, showModalWindow, userLoginEvent, addPatterntHTMLEvent , userExit} from "./events.js";
import { getLogin, getPassword } from "./methods.js";

//Перевірка авторизації користувача. 
if (!sessionStorage.isLoginUser && !document.location.pathname.includes("/authorization/")) {
   document.location = "/authorization"
}

// Подія "change" для полів авторизації.
try {
   document.querySelector(".window form")
      .addEventListener("change", changeInputAuth);
   document.querySelector("#btn")
      .addEventListener("click", userLoginEvent)
} catch (e) {
   if (document.location.pathname.includes("/authorization/")) {
      console.error(e)
   }
}

// Подія клік для кнопки "Додати щось нове."
try{
   document.getElementById("btn-add-product")
   .addEventListener("click", showModalWindow);
   
   document.getElementById("btn-add-product")
   .addEventListener("click", addPatterntHTMLEvent);
   
}catch (e){
   
}

// Подія клік для кнопки "Вийти".
try{
   document.querySelector(".exit")
   .addEventListener("click", ()=>{
      userExit()
   });
}catch (e) {
   if (!document.location.pathname.includes("/authorization/")) {
      console.error(e)
   }
}

if(!localStorage.restorationBD ){
   localStorage.restorationBD = JSON.stringify([])
}
if(!localStorage.storeBD ){
   localStorage.storeBD = JSON.stringify([])
}
if(!localStorage.videoBD ){
   localStorage.videoBD = JSON.stringify([])
}



console.log(getLogin())
console.log(getPassword());

