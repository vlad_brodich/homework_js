import { hideModalWindowEvent, showModalWindow } from "./events.js";
import { createEditProductInput, createProductElement,showAlertOk } from "./methods.js";

// Перевірка поточноі сторінки.
const restoran = document.location.pathname.includes("/restoran/"),
store = document.location.pathname.includes("/store/"),
video = document.location.pathname.includes("/video/");


// Функція виводу данних в таблицю.
function showListProduct() {
    const tbody = document.querySelector('tbody');
    // Отримання локальних данних в залежності від сторінки на якій знаходимось.
    let data = ''
   if(restoran) {
    data =  JSON.parse(localStorage.restorationBD);
   }
   else if(store){
    data = JSON.parse(localStorage.storeBD);
   }
   else if(video){
    data = JSON.parse(localStorage.videoBD);
   }
    // Перевірка на те чи отримали ми масив
    if (!Array.isArray(data))
    throw new Error("Ми отримали не масив!!!");

    const table = document.querySelector("table");
    tbody.innerHTML = "";

    const tableD = data.map((e, i) => {
        const { productName, videoName, quantity, date, videoUrl, price, porductPrice
        } = e;
        let tds=[]
        const tr = createProductElement("tr");
        if(video){
            // Набір елементів таблиці для стрінки "Відио хостинг".
            tds = [
                createProductElement("td", undefined, i + 1),
                createProductElement("td", undefined, videoName),
                createProductElement("td", undefined, date),
                createProductElement("td", undefined,`<a href="${videoUrl}"target="_blank">${videoUrl}</a>`),
                createProductElement("td", undefined, "<span class='icon'>&#128221;</span>", undefined, editClickEvent, e),
                createProductElement("td", undefined, "<span class='icon'>&#128465;</span>",undefined,editClickEventRemove, e)
            ] 
        }else{
             // Набір елементів таблиці для стрінок "Ресторан" та "Магазин".
            tds = [
                createProductElement("td", undefined, i + 1),
                createProductElement("td", undefined, productName),
                createProductElement("td", undefined, quantity),
                createProductElement("td", undefined, price || porductPrice),
                createProductElement("td", undefined, "<span class='icon'>&#128221;</span>", undefined, editClickEvent, e),
                createProductElement("td", undefined, quantity > 0 ? "&#9989;" : "&#10060;"),
                createProductElement("td", undefined, date),
                createProductElement("td", undefined, "<span class='icon'>&#128465;</span>",undefined,editClickEventRemove, e)
            ]
        }
        tr.append(...tds);
        return tr
    })

    tbody.append(...tableD);
}
showListProduct();


// Функція події клік редагування товарів.
function editClickEvent(e) {
    // e - event || productObject
    const mw = document.querySelector(".modal-window");
    // Очищення модалки.
    mw.innerHTML =''
    const product = Object.entries(e).map(([key, value], id) => {
        return createEditProductInput(value, key, id)
    });

    const div = createProductElement("div", "btn-edit-product");
    const save = createProductElement("button", "save-product", "Зберегти", undefined, saveProduct, e);
    div.append(save)

    mw.append(...product, div);
    showModalWindow()

}

// Функція внесення змін до товару.
function saveProduct(oldObject) {
    hideModalWindowEvent()
    const newObj = {
        id: oldObject.id,
        date: oldObject.date,
    }

    const inputs = document.querySelectorAll(".modal-window input");

    inputs.forEach((el) => {
        if (el.key === "stopList") return
        newObj[el.key] = el.value;
    })
    newObj.stopList = newObj.quantity > 0 ? false : true;

    let arr = {}
   if(restoran) {
    arr =  JSON.parse(localStorage.restorationBD);
   }
   else if(store){
    arr = JSON.parse(localStorage.storeBD);
   }
   else if(video){
    arr = JSON.parse(localStorage.videoBD);
   }
    const index = arr.findIndex((el) => {
        return el.id === oldObject.id
    })
    arr.splice(index, 1, newObj);
    if(restoran) {
        localStorage.restorationBD = JSON.stringify(arr);
        showAlertOk("Ви успішно зберегли зміни про страву!")
       }
       else if(store){
        localStorage.storeBD = JSON.stringify(arr);
        showAlertOk("Ви успішно зберегли зміни про товар!")
       }
       else if(video){
        localStorage.videoBD = JSON.stringify(arr);
        showAlertOk("Ви успішно зберегли зміни про відео!")
       }
    showListProduct()
}

// Функція події клік видалення товарів.
function editClickEventRemove(e){
    const mw = document.querySelector(".modal-window");
    // Очищення модалки.
    mw.innerHTML =''
    const txt = createProductElement("h2");
    txt.innerHTML = `Ви впевненні що хочите видалити цю позицію?<br>Id: ${e.id}<br>Назва: ${e.productName} `;
    const div = createProductElement("div", "btn-edit-product");
    const remove = createProductElement("button", "save-product", "Видалити", undefined, removeProduct, e);
    const cancel = createProductElement("button", "save-product", "Скасувати", undefined, cancelProduct);
    div.append(remove,cancel)
    mw.append(txt, div);
    showModalWindow()
};

// Функція видалення товару.
function removeProduct(e){
    let arr = {}
   if(restoran) {
    arr =  JSON.parse(localStorage.restorationBD);
   }
   else if(store){
    arr = JSON.parse(localStorage.storeBD);
   }
   else if(video){
    arr = JSON.parse(localStorage.videoBD);
   }
   const index = arr.findIndex((elem) => {
    return elem.id === e.id})
    arr.splice(index, 1);
    if(restoran) {
        localStorage.restorationBD = JSON.stringify(arr);
        showAlertOk("Ви успішно видалили страву!")
       }
       else if(store){
        localStorage.storeBD = JSON.stringify(arr);
        showAlertOk("Ви успішно видалили товар!")
       }
       else if(video){
        localStorage.videoBD = JSON.stringify(arr);
        showAlertOk("Ви успішно видалили відео!")
       }
    showListProduct()
    hideModalWindowEvent()
};

// Функція скасування видалення.
function cancelProduct(){
    hideModalWindowEvent()
};