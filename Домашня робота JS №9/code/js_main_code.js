'use strict'// Суворий режим

// Оголошення функцій

// Функція для випадаючого меню 
function showMenu() {
    document.getElementById("myDropdown").classList.toggle("show");
};
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    let dropdowns = document.getElementsByClassName("dropdown-content");
    for (let i = 0; i < dropdowns.length; i++) {
      let openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      };
    };
  };
};

document.getElementById("btnShowMenu").onclick = (e => showMenu());
















// Приклади з класної роботи №8

/* Функція виводу контенту на сторінку: 
 Приймає параметри: 
 Увага!!! як що передаєте CSS-class, слідкуйте щоб він не був один на сторінці
 1. id або CSS-class елемента = "Куди потрібно вивести контент",
 2. text = Текст або елемент який потрібно вивести,
 3. position:
   "afterBegin" - у вказаний елемент на початок.
   "beforeEnd"- у вказаний елемент в кінець. - Встановлений за замовчуванням.
   "afterEnd"- після вказаного елементу.
   "beforeBegin'- перед вказаним елементом. */
   function showResult(element,content = '',position = "beforeEnd"){
    let str = element.split("");
    if(str[0] === '.'){
        document.querySelector(element).insertAdjacentHTML(`${position}`,
        content);
    }
    else {
        document.getElementById(element).insertAdjacentHTML(`${position}`,
        content);
    }  
};






// Нижче наведено більше методів для вставки, вони вказують куди саме буде вставлено вміст:

// node.append(...вузли або рядки) – додає вузли або рядки в кінець node,
// node.prepend(...вузли або рядки) – вставляє вузли вбо рядки на початку node,
// node.before(...вузли або рядки) – вставляє вузли або рядки попереду node,
// node.after(...вузли або рядки) – вставляє вузли або рядки після node,
// node.replaceWith(...вузли або рядки) – замінює node заданими вузлами або рядками.



// window.onload = () => {}; /*Функція затримує виконання подальшого коду до завантаження сторінки */

 /*Функція знаходить елемент по id та обробляє подію "onclick-натискання"*/
// document.getElementById("id який треба знайти").onclick = (e) => {

//   const [...radioButtons] = document.getElementsByName("ім'я елементів");

//   let res = '';
//   radioButtons.forEach((item, i, a) => {
//     res += `Перше значення ${item.value} Друге значення ${item.checked}`
//   });
// };

 /*Отримання всіх елементів по тегу та додавання інлайн стилів*/
  // const [...tags] = document.getElementsByTagName('p');

  // tags.forEach(element => {
  //   element.style.color = 'red'
  // });
/*Отримання всіх елементів по CSS класу та додавання інлайн стилів*/

    // const [...clases] = document.getElementsByClassName('назва класу');
    // clases.forEach(e => e.style.color = 'red');

// створення та додавання тегів на сторінку
// const div = document.getElementById('homework-body-js');
// const p1 = document.createElement('p');
// p1.textContent = 'text';
// p1.classList.add('class');

// div.append(p1);//Ставить елемент в кінець 
// div.prepend(p1)
// document.body.prepend(p1) //Ставить елемент з початку 


// Домашня робота №8

/*Завдання:
1. Намалювати на сторінці коло за допомогою параметрів, які введе користувач.
2. При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло".
 Дана кнопка повинна бути єдиним контентом у тілі HTML документа,решта контенту повинен бути створений і доданий на сторінку за допомогою Javascript.
3. При натисканні кнопки "Намалювати коло" показувати одне поле введення - діаметр кола.
4. При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору.
5. При натисканні на конкретне коло - це коло повинене зникати, при цьому порожнє місце заповнюватися, тобто всі інші кола зрушуються вліво.
Термін виконання до ЧТ */
