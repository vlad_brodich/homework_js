'use strict'// Суворий режим

// Домашня робота №9
/* 1. Згенерувати теги html через javascript.
2. Додати на сторінку семантичні теги та метатеги опису сторінки.
3. Прописати стилі для для елементів використовуючи css, id та класи.
4. При натиску на тег ми можемо додати будь-який контент,  він збережеться в тегу та відобразиться на сторінці. */

// Створення та додавання html розмітки сторінки різними методами

// Вивід метатегів в head HTML розмітки сторінки.
document.querySelectorAll("title").forEach((item) => {
  // Підключення homework_style.css.
    item.insertAdjacentHTML('afterEnd',`<link rel="stylesheet" href="../style/homework_style.css">`);

  // Підключення reset.css.
    item.insertAdjacentHTML('afterEnd',`<link rel="stylesheet" href="../style/reset.css">`);

  // Підключення шрифтів.
    item.insertAdjacentHTML('afterEnd',`<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">`);

  // Вивід метатегa "description" в head HTML розмітку сторінки.
    item.insertAdjacentHTML('afterEnd',`<meta name="description" content="Школа "Philip Sevene", Доступне навчання ІТ Курси 'Front-end' (HTML,CSS,JS),Пишіть нам :philip@7e.com.ua." />`);
});

// Фукція створення  тегів
function creationElemPage(teg,clasCss,textEl){
    let elem = document.createElement(teg);
    elem.classList.add(clasCss);
    elem.textContent = textEl; 
    return elem;
};

// Фукція створення вкладених тегів
function creatNodes (parent,cild,clasCss){
    let clas = clasCss;
    const nod = parent;
    if(clasCss !== undefined){ nod.classList.add(clas);}
    nod.append(cild);
    return nod
};

// Шапка
const header = creationElemPage('header','header','HEADER');
const nav = creationElemPage('nav','header__nav','NAV');

// Секція 1
const section1 = creationElemPage('section','section-1','SECTION');
// section1.classList.add('js-el');
section1.innerHTML = `SECTION
<header class="section-1__header">HEADER</header>
<article class="section-1__article-1">
  ARTICLE
  <header class="article-1__header">HEADER</header>
  <p class="article-1__text">P</p>
  <div class="article-1__box">
    <p  class="article-1__box__text">P</p>
    <aside class="article-1__box__aside">ASIDE</aside>
  </div>
  <footer class="article-1__footer">FOOTER</footer>
</article>
<article class="section-1__article-2">
    ARTICLE
  <header class="article-2__header">HEADER</header>
  <p class="article-2__text-1">P</p>
  <p class="article-2__text-2">P</p>
  <footer class="article-2__footer">FOOTER</footer>
</article>
<footer class="section-1__footer">FOOTER</footer>`;

// Секція 2
const section2 = creationElemPage('section','section-2','SECTION');
const navSection2 = creationElemPage('nav','section-2__nav','NAV');

// Головний контент
const main = creationElemPage('main','main-content','');
main.classList.add('js-el');

main.append(section1);
main.append(creatNodes (section2,navSection2,));

// Футер
const footer = creationElemPage('footer','footer','FOOTER');
footer.classList.add('js-el');

// Вивід HTML розмітки на сторінку
document.querySelectorAll('body').forEach((item) => {
    item.insertAdjacentHTML('afterBegin',` <div class="modal" id="modal">
    <form class="modal-form">
        <p&gt;>За допомогою цієї форми у вас є можливість створити та опублікувати власний контент на цій сторінці<br>Наприклад напишіть текст або додайте html теги.<br>Цей приклад ( &lt;img src="../imege/logo.jpg" alt="logo" width="60"&gt; ) - додасть зображення.<br>  В опубліковану розмітку можна ще раз додавати контент.<br> Приклад: &lt;p class="article-2__text-2"&gt;Тест&lt;/p&gt;
        </p>
        <textarea id="text" cols="30" rows="10"></textarea>
        <button class="modal-form__btn" id="modal-btn" type="button">Опублікувати текст</button>
    </form>
  </div>`);
    item.prepend(footer);
    item.prepend(main);
    item.prepend(creatNodes (header,nav,'js-el'));
});


// Функціонал сторінки

// Оголошення змінних модального вікна    
const modal = document.getElementById('modal');
const modalBtn = document.getElementById('modal-btn');

// Функція виводу модального вікна та блокування сторінки від скролу
function showModal (){
   modal.classList.add('show-modal');
   document.body.classList.add('stop-overflow');
};

 // Функція закриття модального вікна та разблокування сторінки
function removeModal (){
    modal.classList.remove('show-modal');
    document.body.classList.remove('stop-overflow');
}; 

// Функція виділяє обраний елемент  
let selectedEl;
function highlight(el) {
    if (selectedEl) { // видалити наявне виділення, якщо таке є
        selectedEl.classList.remove('highlight');
    }
    selectedEl = el;
    selectedEl.classList.add('highlight'); // виділити новий el
};

// Пошук основних вузлів DOM
const [...elementsList] = document.querySelectorAll(".js-el");

// Функція перевіряє який елемент обрав користувач
let element;

elementsList.forEach((item) => {
    item.onclick = function(event) {
        element = event.target;
        showModal ();
        return highlight(event.target);
    }    
});

// Функція отримуе та публікує контент який ввів користувач
function showContentUser(){
    const content = document.getElementById('text');
    element.insertAdjacentHTML('beforeEnd',`${content.value}`);
    content.value ='';
    removeModal ()
};

// Функція натискання кнопки модального вікна
modalBtn.onclick = (e => showContentUser());