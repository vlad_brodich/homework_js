// Код для випадаючого меню (Цей код взятий з попереднього прєкту та підлаштований під мої потреби але я обов'язково навчусь писати його самостійно)
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
// Домашня робота №3
// Оголошення змінних
let arrFirst = [];
let arrSecond = [];
let result = [];
// Приклад 1
//Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. Об'єднайте їх разом.
arrFirst = ['a', 'b', 'c'];
arrSecond = [1, 2, 3];
//Рішення
result = result.concat(arrFirst ,arrSecond);
// Вивід результату на сторінку
document.querySelector('.box-1').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 arrFirst = ['a', 'b', 'c'];<br>
 arrSecond = [1, 2, 3];<br>
 result = result.concat(arrFirst ,arrSecond);<br>
Елементи об'єднаного масиву: <span>[ ${result} ].</span></p>`);
// Приклад 2
//Дан масив ['a', 'b', 'c']. Додайте йому до кінця елементи 1, 2, 3.
arrFirst = ['a', 'b', 'c'];  
//Рішення
arrFirst.push(1,2,3);
// Вивід результату на сторінку
document.querySelector('.box-2').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
arrFirst = ['a', 'b', 'c'];<br>
arrFirst.push(1,2,3);<br>
Елементи масиву після додавання: <span>[ ${arrFirst} ].</span></p>`);
// Приклад 3
// Дан масив [1, 2, 3]. Зробіть із нього масив [3, 2, 1].
arrSecond = [1, 2, 3];
//Рішення
arrSecond.reverse();
// Вивід результату на сторінку
document.querySelector('.box-3').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
arrSecond = [1, 2, 3];<br>
arrSecond.reverse();<br>
Елементи масиву після виконання коду: <span>[ ${arrSecond} ].</span></p>`);
// Приклад 4
// Дан масив [1, 2, 3]. Додайте йому до кінця елементи 4, 5, 6.
arrSecond = [1, 2, 3];
//Рішення
arrSecond.push(4,5,6);
// Вивід результату на сторінку
document.querySelector('.box-4').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 arrSecond.push(4,5,6);<br>
Елементи масиву після додавання: <span>[ ${arrSecond} ].</span></p>`);
// Приклад 5
// Дан масив [1, 2, 3]. Додайте йому на початок елементи 4, 5, 6.
arrSecond = [1, 2, 3];
//Рішення
arrSecond.unshift(4,5,6);
// Вивід результату на сторінку
document.querySelector('.box-5').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 arrSecond = [1, 2, 3];<br>
 arrSecond.unshift(4,5,6);<br>
Елементи масиву після додавання: <span>[ ${arrSecond} ].</span></p>`);
// Приклад 6
// Дан масив ['js', 'css', 'jq']. Виведіть перший елемент на екран.
arrFirst = ['js', 'css', 'jq'];
//Рішення
arrFirst[0];
// Вивід результату на сторінку
document.querySelector('.box-6').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 arrFirst = ['js', 'css', 'jq'];<br>
 arrFirst[0];<br>
Перший елемент масиву: <span> ${arrFirst[0]} .</span></p>`);
// Приклад 7
// Дан масив [1, 2, 3, 4, 5]. За допомогою методу slice запишіть нові елементи [1, 2, 3].
arrSecond = [1, 2, 3, 4, 5];
result = [];
//Рішення
result = arrSecond.slice(0,3);
// Вивід результату на сторінку
document.querySelector('.box-7').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 arrSecond = [1, 2, 3, 4, 5];<br>
 result = arrSecond.slice(0,3);// починаючи з позиції 0, копіювати елементи до позиції 3 не включно<br>
Копія масиву з вказаними в умові елементами: <span>[ ${result} ].</span></p>`);
// Приклад 8
//  Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5].
arrSecond = [1, 2, 3, 4, 5];
//Рішення
arrSecond.splice(1,2);
// Вивід результату на сторінку
document.querySelector('.box-8').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 arrSecond = [1, 2, 3, 4, 5];<br>
 arrSecond.splice(1,2);// починаючи з позиції 1, видалити 2 елементи<br>
Масив після видалення вказаних елементів: <span>[ ${arrSecond} ].</span></p>`);
// Приклад 9
//  Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 2, 10, 3, 4, 5]
arrSecond = [1, 2, 3, 4, 5];
//Рішення
arrSecond.splice(2,0,10);
// Вивід результату на сторінку
document.querySelector('.box-9').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 arrSecond = [1, 2, 3, 4, 5];<br>
 arrSecond.splice(2,0,10);// починаючи з позиції 2, видалити 0 елементів,додати елемент 10<br>
Масив після додавання вказаних елементів: <span>[ ${arrSecond} ].</span></p>`);
// Приклад 10
//  Дан масив [3, 4, 1, 2, 7]. Відсортуйте його.
arrSecond = [3, 4, 1, 2, 7];
//Рішення
arrSecond.sort();
// Вивід результату на сторінку
document.querySelector('.box-10').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 arrSecond = [3, 4, 1, 2, 7];<br>
 arrSecond.sort();<br>
Масив після сортування елементів: <span>[ ${arrSecond} ].</span></p>`);
// Приклад 11
//  Дан масив з елементами 'Привіт, ', 'світ' і '!'. Потрібно вивести на екран фразу 'Привіт, мир!'.
arrFirst = ['Привіт, ', 'світ', '!'];
//Рішення
arrFirst.splice(1,1,"мир");
result = arrFirst.join(' ');
// Вивід результату на сторінку
document.querySelector('.box-11').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 arrFirst = ['Привіт, ', 'світ', '!'];<br>
 arrFirst.splice(1,1,"мир");<br>
 result = arrFirst.join(' ');<br>
Повідомлення: <span>' ${result} '.</span></p>`);
// Приклад 12
//  Дан масив ['Привіт, ', 'світ', '!']. Необхідно записати в нульовий елемент цього масиву слово 'Поки, ' (тобто замість слова 'Привіт, ' буде 'Поки, ').
arrFirst = ['Привіт, ', 'світ', '!'];
//Рішення
arrFirst[0] = "Поки,";
result = arrFirst.join(' ');
// Вивід результату на сторінку
document.querySelector('.box-12').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 arrFirst = ['Привіт, ', 'світ', '!'];<br>
 arrFirst[0] = "Поки,";<br>
 result = arrFirst.join(' ');<br>
 Масив піясля зміни елемента: <span>[ ${arrFirst} ].</span><br>
Повідомлення: <span>' ${result} '.</span></p>`);
// Приклад 13
//  Створіть масив arr з елементами 1, 2, 3, 4, 5 двома різними способами.
//Рішення
const arr1 = [1, 2, 3, 4, 5];
const arr2 = new Array(1, 2, 3, 4, 5);
// Вивід результату на сторінку
document.querySelector('.box-13').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 const arr1 = [1, 2, 3, 4, 5]; //Створення масиву за допомогою квадратних дужок<br>
 const arr2 = new Array(1, 2, 3, 4, 5); //Створення масиву за допомогою команди: new Array()<br>
 Масив arr1: <span>[ ${arr1} ].</span><br>
 Масив arr2: <span>[ ${arr2} ].</span></p>`);
 // Приклад 14
//  Дан багатовимірний масив arr:Виведіть за його допомогою слово 'блакитний' 'blue' 
var arr = {
    'ru':['блакитний', 'червоний', 'зелений'],
    'en':['blue', 'red', 'green'],
};
//Рішення
result = `'${arr.ru[0]}'  '${arr.en[0]}'`;
// Вивід результату на сторінку
document.querySelector('.box-14').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 var arr = {
    'ru':['блакитний', 'червоний', 'зелений'],
    'en':['blue', 'red', 'green'],
}; - це об'єкт з вкладеними масивами.<br>
Тому звернення до елеметів виглядає так: об'єкт.ключ[індекс].<br>
arr.ru[0]<br>
arr.en[0]<br>
 Повідомлення: <span>${result}.</span><br>
 </p>`);
 arrFirst = [ru = ['блакитний', 'червоний', 'зелений'], en = ['blue', 'red', 'green']];
 //Рішення
 result = `'${arrFirst[0][0]}'  '${arrFirst[1][0]}'`;
 // Вивід результату на сторінку
 document.querySelector('.box-14').insertAdjacentHTML("beforeend",
 `<p>Виконання варіант 2:<br>
 arrFirst = [ru = ['блакитний', 'червоний', 'зелений'], en = ['blue', 'red', 'green']]; - це  багатовимірний масив.<br>
Тому звернення до елеметів виглядає так: масив.[індекс- основного масиву][індекс- вкладеного масиву].<br>
arrFirst[0][0];<br>
arrFirst[1][0];<br>
 Повідомлення: <span>${result}.</span><br>
 </p>`);
 // Приклад 15
//  Створіть масив arr = ['a', 'b', 'c', 'd'] і за допомогою його виведіть на екран рядок 'a+b, c+d'. 
//Рішення
const arr3 = ['a', 'b', 'c', 'd'];
result = `${arr3[0]} + ${arr3[1]}, ${arr3[2]} + ${arr3[3]}`;
// Вивід результату на сторінку
document.querySelector('.box-15').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 const arr3 = ['a', 'b', 'c', 'd'];<br>
 Повідомлення: <span>'${result}'.</span><br>
 </p>`);
  // Приклад 16
// Запитайте у користувача кількість елементів масиву.
// Виходячи з даних, які ввів користувач створіть масив на ту кількість елементів, яку передав користувач.
// У кожному індексі масиву зберігайте чило який показуватиме номер елемента масиву. 
//Рішення
let number = parseInt( prompt("Вкажіть будь ласка кількість елементів масиву який ви хочете створити?",0));
if(number <= 0){
    number = 5;   
}
const arrUser = new Array(number);
for(let i = 0; i < number; i +=1){
    arrUser[i] = i;
    if(i % 10 === 0 && i > 0){
        arrUser[i] = `${i}<br>`
    }
}
// Вивід результату на сторінку
document.querySelector('.box-16').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 Число яке ввів користувач: <span>${number} .</span>або за замовчуванням 5<br>

 Елементи створеного  масиву: [ ${arrUser} ].
 </p>`);
   // Приклад 17
// Зробіть так, щоб з масиву, який ви створили вище, вивелися всі непарні числа в параграфі, а парні в спані з червоним тлом. 
//Рішення
for(let i = 0; i < number; i +=1){
    arrUser[i] = i;
    if(i % 10 === 0 && i % 2 === 0 && i > 0){
        arrUser[i] = `<span class="red">${i}</span><br>`; 
    }
    else if(i % 2 === 0){
        arrUser[i] = `<span class="red">${i}</span>`; 
    }
    else if(i % 2 === 1){
        arrUser[i] = `<span class="green">${i}</span>`;
    }
}
// Вивід результату на сторінку
document.querySelector('.box-17').insertAdjacentHTML("beforeend",
 `<p>Виконання:<br>
 <span span class="red">Парні</span> та <span span class="green">не парні</span> елементи масиву: [ ${arrUser} ]
 </p>`);
    // Приклад 18
// Напишіть код, який перетворює та об'єднує всі елементи масиву в одне рядкове значення. Елементи масиву будуть розділені комою. 
//Рішення
var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];
    
result = String(vegetables);


// document.write(str1); // "Капуста, Ріпа, Редиска, Морквина"
// Вивід результату на сторінку
document.querySelector('.box-19').insertAdjacentHTML("beforeend",
 `<p>result = String(vegetables);
 </p>`);
 document.querySelector('.box-18').insertAdjacentHTML("beforeend",
 `<p>Результат виконання коду: <span>" ${result} "</span>
 </p>`);