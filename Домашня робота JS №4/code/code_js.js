'use strict'// Суворий режим

// Оголошення змінних
let arrFirst = ['a', 'b', 'c',3,5,7,9,"test"];;
let arrSecond = [1, 3,2,4,6,"test",22,8,7,9,10,50];
let price = [10,15,30,50,100,110,2,5,90,88,70];

// Оголошення функцій

// Функція для випадаючого меню 
function showMenu() {
    document.getElementById("myDropdown").classList.toggle("show");
}
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    let dropdowns = document.getElementsByClassName("dropdown-content");
    for (let i = 0; i < dropdowns.length; i++) {
      let openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

// Функція виводу данних на сторінку selector = сclass="Куди потрібно вивести",text = Текст який потрібно вивести.
function showResult(selector,text = `<p>Результат виконання: <span class="red">Не виконано</span></p>`){
  document.querySelector(selector).insertAdjacentHTML("beforeend",
  text);
}

//Приклад 1 переписати функцію
function checkAge(age) {
  if (age > 18) {
    return true;
  } else {
    return confirm('Батьки дозволяють перевіряти домашні роботи?');
  }
}
function checkAgeTest1(age) {
  return (age > 18) || confirm('Батьки дозволяють перевіряти домашні роботи?');
}
function checkAgeTest2(age) {
  return (age > 18) ? true : confirm('Батьки дозволяють перевіряти домашні роботи?');
}


// Функція перевіряє масив на вміст парних чисел
function createArrNumbEven(arr){
  let result = [];
  for (let key of arr) {
    if(key % 2 === 0){
      result.push(key);
    }
  }
  if(result.length === 0){
    result = 'В масиві немає парних чисел';
  }
  return result;
}

// Функція перевіряє масив прайс де до кожної ціни додає ПДВ 20%
function createArrPriceInterest(arr){
  let result = [];
  for (let key of arr){
    if (key > 0) {
      result.push(key+=key*0.2);
    }
  }
  return result;
}

// Функція приймає масив та функцію яка щось робить з масивом
function map(fn, array){
  let result = [];
  result = fn(array);
  return result;
}

// Калькулятор
function add(a,b,c) {
  if(c === b){
    return a / 100 * b + a;
  }
  else return a + b;
}

function multiple(a,b,c) {
  if(c === b){
    return a / 100 * b;
  }
  else return a * b;
}

function mul(a,b,c) {
  if (b == 0){
    return "Ділення на 0 не припустимо";
  }
  else if(c === b){
    return a / (b / 100 * a);
  }
  else return a / b;
}

function minus(a,b,c) {
  if(c === b){
    return a - (b / 100 * a);
  }
  else return a - b;
}

function calculate(){
  const elm_1 = document.getElementsByName('number-1')[0];
  const a = parseFloat(elm_1.value);
  const elm_2 = document.getElementsByName('number-2')[0];
  const b = parseFloat(elm_2.value);
  const action_1 = document.getElementById('add');
  const action_2 = document.getElementById('minus');
  const action_3 = document.getElementById('multiple');
  const action_4 = document.getElementById('mul');
  const action_5 = document.getElementById('rate');
  let c ='';
  let rate = '';
  if(isNaN(a) || isNaN(b)){
    showResult('.box-5',`<p><span class="red">Введіть число</span></p>`);
  }
  else{
    if(action_5.checked  === true){
      if(b < 0){
        showResult('.box-5',`<p><span class="red"> ${b} % - Не припустиме значення <br></span> Виконується тількі наступна дія</p>`);
      }
      else{
        c = b;
        rate = '%';
      }
    }
    if(action_1.checked  === true){
        showResult('.box-5',`<p>Результат виконання: ${a} + ${b}${rate} = <span class="red"> ${(add(a,b,c)).toFixed(2)}</span></p>`);
    }
    else if(action_2.checked  === true){
        showResult('.box-5',`<p>Результат виконання: ${a} &#8722; ${b}${rate} = <span class="red"> ${(minus(a,b,c)).toFixed(2)}</span></p>`);
    }
    else if(action_3.checked  === true){
        showResult('.box-5',`<p>Результат виконання: ${a} × ${b}${rate} = <span class="red"> ${(multiple(a,b,c)).toFixed(2)}</span></p>`);
    }
    else if(action_4.checked  === true){
        showResult('.box-5',`<p>Результат виконання: ${a} ÷ ${b}${rate} = <span class="red"> ${(mul(a,b,c)).toFixed(2)}</span></p>`);
    }
    else showResult('.box-5',`<p>Результат виконання: <span class="red">Помилка</span></p>`);
  }
}

// Виклик функцій

// Приклад 1
checkAgeTest1(parseInt(prompt("Скільки Вам років?", 18)));

// Приклад 2
showResult('.box-3',`<p>Результат виконання: <span class="red">[ ${map(createArrNumbEven,arrSecond)} ]</span></p>`);

// Приклад 3
showResult('.box-4',`<p>Результат виконання: <span class="red">[ ${map(createArrPriceInterest,price)} ]</span></p>`);