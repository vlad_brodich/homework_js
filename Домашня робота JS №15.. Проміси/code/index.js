'use strict'// Суворий режим
import { get, showSlides} from "./functions.js";
// Оголошення функцій

// Функція згортання випадаючого меню. 
window.addEventListener ('click',(event) => {
  if (event.target !== get("btnShowMenu")) {
    get("myDropdown").classList.remove("show");
  };
});

// Кнопка випадаючого меню. 
get("btnShowMenu").addEventListener ('click', () => {get("myDropdown").classList.add("show")});

// Виклик функції "слайдер" після завантаження сторінки.
window.addEventListener('load',showSlides);