'use strict'// Суворий режим.

// Функція пошуку елемента по id.
const get= id => document.getElementById(id);

// Оголошення змінних "слайдера".
let slideIndex = 0;
const slides = document.getElementsByClassName("slide");

// Функція "слайдер" змінює картинки кожні 5 секунд.
const showSlides = () => {
  for (let i = 0; i < slides.length; i++) {
      slides[i].classList.remove('show-slide');  
  }
  slideIndex++;
  if (slideIndex === slides.length) {
      slideIndex = 0;
  } 
  slides[slideIndex].classList.add('show-slide');
  // Таймер "слайдера".
  setTimeout(showSlides, 5000); 
};

// Функція виводу вікна помилки.
function errorWindow (aStat,statText){
  const el = document.createElement('div');
  el.className = "error-box";
  el.innerHTML = `<p>Помилка у запиті на сервер : ${aStat} / ${statText}</p>`
  document.body.append(el);
  const remove =()=>{el.remove(el)};
  setInterval(remove,5000);
}

// Функція запиту на сервер.
function getServer(url, callback = () => {}) {
  const ajax = new XMLHttpRequest();
  ajax.open("get", url);
  ajax.send();
  ajax.addEventListener("readystatechange", () => {
      if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
        callback(JSON.parse(ajax.response))
      } else if (ajax.readyState === 4) {
        errorWindow (ajax.status,ajax.statusText)
        document.querySelector(".box-loader").classList.remove("show-loader");
      };
  });
};

// Функція створення елементів.
const elConstr = (el,clas)=>{
  let htmEl = document.createElement(el);
  htmEl.classList = clas;
  return htmEl;
};

export{getServer,errorWindow,get,showSlides,elConstr};