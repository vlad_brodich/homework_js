'use strict'// Суворий режим
import {get,getServer} from "./functions.js";

// Функція згортання випадаючого меню. 
window.addEventListener ('click',(event) => {
  if (event.target !== get("btnShowMenu")) {
    get("myDropdown").classList.remove("show");
  };
});

// Кнопка випадаючого меню. 
get("btnShowMenu").addEventListener ('click', () => {get("myDropdown").classList.add("show")});

/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 

Курс валют НБУ з датою на який день, 

героїв зоряних війн, 

список справ з https://jsonplaceholder.typicode.com/ 
//виводити які з можливістю редагування при натискані 
*/

const req = async (url) => {
  document.querySelector(".box-loader").classList.add("show-loader")
  const data = await fetch(url);
  return await data.json();
}

const nav = document.querySelector(".nav")
  .addEventListener("click", (e) => {
    if (e.target.dataset.link === "nbu") {
      req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
        .then((info) => {
            show(info)
          });
    get('title').innerText = 'Назва валюти.';
    get('info-1').innerText = 'Курс.';
    get('info-2').innerText = 'Дата.'; 
    } 
    else if (e.target.dataset.link === "star") {
      const promise = new Promise((resolve) => {
        const planets = [];
        for(let i = 1; i <= 6; i++){
          req(`https://swapi.dev/api/planets/?page=${i}`)
            .then((info) => {
                info.results.forEach(el =>{ 
                  planets.push(el)   
                })
              if(planets.length ===  info.count){resolve (planets);}
            });
          }   
      });
      promise
        .then(result => show(result));
      get('title').innerText = 'Назва планети.';
      get('info-1').innerText = 'Діаметр.';
      get('info-2').innerText = 'Клімат.'; 
    } 
    else if (e.target.dataset.link === "todo") {
        req("https://jsonplaceholder.typicode.com/todos")
        .then((info) => {
            show(info)
        });
        get('title').innerText = 'Запланована справа.';
        get('info-1').innerText = 'Зроблено або ні.';
        get('info-2').innerText = 'Примітка.';
    } 
  });

function show(data = []) {
  if (!Array.isArray(data)) return;
  const tbody = document.querySelector("tbody");
  tbody.innerHTML = ""
  const newArr = data.map(({txt, rate, exchangedate, title, completed,name,diameter,climate }, i) => {
      return {
        id: i + 1,
        name : txt || title || name,
        info1 : rate || diameter || completed,
        info2 : exchangedate || climate || "-",
      }
  });

  newArr.forEach(({ name, id, info1, info2 }) => {
      tbody.insertAdjacentHTML("beforeend", `
      <tr> 
      <td>${id}</td>
      <td>${name}</td>
      <td>${info1}</td>
      <td>${info2}</td>
      </tr>
      `)
  })

  document.querySelector(".box-loader").classList.remove("show-loader")
}
