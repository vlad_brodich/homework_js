'use strict'// Суворий режим

// Оголошення функцій

// Функція пошуку елемента по id
const get = id => document.getElementById(id);

// Функція згортання випадаючого меню 
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    const [...dropdowns] = document.getElementsByClassName("dropdown-content");
    dropdowns.forEach((item)=>{
      if (item.classList.contains('show')){
        item.classList.remove('show');
      };
    });
  };
};

// Кнопка випадаючого меню 
get("btnShowMenu").onclick = () => {get("myDropdown").classList.toggle("show")};






// Приклади з класної роботи №8


// window.onload = () => {}; /*Функція затримує виконання подальшого коду до завантаження сторінки */

 /*Функція знаходить елемент по id та обробляє подію "onclick-натискання"*/
// document.getElementById("id який треба знайти").onclick = (e) => {

//   const [...radioButtons] = document.getElementsByName("ім'я елементів");

//   let res = '';
//   radioButtons.forEach((item, i, a) => {
//     res += `Перше значення ${item.value} Друге значення ${item.checked}`
//   });
// };

 /*Отримання всіх елементів по тегу та додавання інлайн стилів*/
  // const [...tags] = document.getElementsByTagName('p');

  // tags.forEach(element => {
  //   element.style.color = 'red'
  // });
/*Отримання всіх елементів по CSS класу та додавання інлайн стилів*/

    // const [...clases] = document.getElementsByClassName('назва класу');
    // clases.forEach(e => e.style.color = 'red');

// створення та додавання тегів на сторінку
// const div = document.getElementById('homework-body-js');
// const p1 = document.createElement('p');
// p1.textContent = 'text';
// p1.classList.add('class');

// div.append(p1);//Ставить елемент в кінець 
// div.prepend(p1)
// document.body.prepend(p1) //Ставить елемент з початку 


// Домашня робота №8

/*Завдання:
1. Намалювати на сторінці коло за допомогою параметрів, які введе користувач.
2. При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло".
 Дана кнопка повинна бути єдиним контентом у тілі HTML документа,решта контенту повинен бути створений і доданий на сторінку за допомогою Javascript.
3. При натисканні кнопки "Намалювати коло" показувати одне поле введення - діаметр кола.
4. При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору.
5. При натисканні на конкретне коло - це коло повинене зникати, при цьому порожнє місце заповнюватися, тобто всі інші кола зрушуються вліво.
Термін виконання до ЧТ */
