'use strict'// Суворий режим
// Домашня робота №8

/*Завдання:
1. Намалювати на сторінці коло за допомогою параметрів, які введе користувач.
2. При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло".
 Дана кнопка повинна бути єдиним контентом у тілі HTML документа,решта контенту повинен бути створений і доданий на сторінку за допомогою Javascript.
3. При натисканні кнопки "Намалювати коло" показувати одне поле введення - діаметр кола.
4. При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору.
5. При натисканні на конкретне коло - це коло повинене зникати, при цьому порожнє місце заповнюватися, тобто всі інші кола зрушуються вліво.
Термін виконання до ЧТ */

// Стилі елементів що не потребують динамічних змін - прописані CSS в файлі.

// Оголошення функцій

// Функція створення контейнера для кульок.
const boxRound = (arg) => {
    const box = document.createElement('div');
    box.classList.add('homework-box');
    box.style.width = `${120 + arg * 10}px`;
    return box;
},

// Функція створення кола різного кольору.
createRound = (width,color = `hsl(${Math.floor(Math.random()*360)},50%,50%)`) => {
    let widthR = width,
    colorB = color,
    round = document.createElement('div');
    round.classList.add('homework-round');
    round.style.width = widthR
    round.style.height = widthR
    round.style.backgroundColor = colorB; 
    return round;
},

// Функція створення посиланнь.
createLink = (colorB,text = 'Почати з початку?',href = './index.html',) => {
    let textT = text,
    hrefL = href,
    linkHom = document.createElement('a');
    linkHom.classList.add('link-hom');
    linkHom.classList.add('hom-focus');
    linkHom.style.backgroundColor = colorB;
    linkHom.textContent = textT;
    linkHom.href = hrefL;
    return linkHom;
},

// Функція видалення кульок після кліку
roundRemove = () => {
const [...clases] = document.getElementsByClassName('homework-round');
clases.forEach((item,i) => {
    item.onclick = () => {
        item.remove(i)
    };
});
},
// Функція створення сотні кульок 
showDrawRound = (arg) => {
    // Диаметр кола за замовчуванням
    let a = arg;
    if(a < 30){
        a = 30;
    }
    else if( 70 < a){
        a = 70;
    };
    document.body.prepend(boxRound(a));
    for(let i = 0; i < 102; i++){
        if(i === 100){
            // Створення кола з посиланням на головну сторінку.
            const linkHom = createLink(`var(--color-FFBB00)`,'На головну','../index.html');
            // Вивід в кінець сторінки кола з посиланням.
            document.querySelector('.homework-box').append(linkHom);
        }
        else if(i === 101){
            // Створення кола з посиланням "оновити сторінку".
            const linkHom = createLink(`var(--color-0be039)`);
                // Вивід в кінець сторінки кола з посиланням.
            document.querySelector('.homework-box').append(linkHom);
        }
        else{
            // Створення кола різного кольору.
            let round = createRound(`${a}px`);
            // Вивід на початок сторінки кола різного кольору.
            document.querySelector('.homework-box').prepend(round)
        }
    };
    roundRemove ();
},

// Функція видалення модального вікна після отримання данних
modalRemove = () => {
    const  numbInput = document.getElementById("modal-input");
    let  [...modal] = document.getElementsByClassName('modal');
    document.getElementById("modal-btn").onclick = (e) =>{
        showDrawRound(numbInput.value);
        modal.forEach((item) => {
            item.remove(item);
        });
    };
},

// Функція створення модального вікна для отримання данних
showModal = () => {
    // Створення модального вікна
    const divModal = document.createElement('div');
    divModal.classList.add('modal');
    document.body.append(divModal);
    // Створення підказки для користувача
    const pModal = document.createElement('p');
    pModal.textContent = 'Вкажіть діаметр кола яке хочите намалювати.(Мін. 30рх.- Мax. 70рх.)';
    divModal.append(pModal);
    // Створення поля вводу отримання данних
    const inputModal = document.createElement('input');
    inputModal.type = 'number';
    inputModal.id = "modal-input";
    inputModal.min ='30';
    divModal.append(inputModal);
    // Створення кнопки 'Намалювати?'
    const btnModal = document.createElement('button');
    btnModal.textContent = 'Намалювати?'
    btnModal.id = "modal-btn";
    btnModal.type = 'button';
    btnModal.classList.add('works-box__btn');
    divModal.append(btnModal);
    modalRemove();
},

// Функція видалення кнопки "Намалювати коло ?" після кліку
btnDraw = document.getElementById("btnDrawRound"),
showDraw = () => {
    showModal()
    btnDraw.remove(btnDraw);
};

// Функція кліку кнопки "Намалювати ?"
document.getElementById("btnDrawRound").onclick = (e => showDraw ());