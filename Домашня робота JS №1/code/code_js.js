// Оголошення змінних
let x = 6;
let y = 14;
let z = 4;
// Отримання данних від користувача
const userName = prompt("Для продовження,Будь ласка заповніть форму","Вкажіть своє ім'я");
const userSurname = prompt("Для продовження,Будь ласка заповніть форму","Вкажіть своє призвище");
const userAge = prompt("Для продовження,Будь ласка заповніть форму","Вкажіть свій вік.");
const number1 = parseInt( prompt("Для продовження,Будь ласка вкажіть три будь які числа","0"));
const number2 = parseInt( prompt("Для продовження,Будь ласка вкажіть три будь які числа","0"));
const number3 = parseInt( prompt("Для продовження,Будь ласка вкажіть три будь які числа","0"));
// Середнє арифметичне
const resultAverageNumber = (number1 + number2 + number3) / 3;

// Вивід домашньої роботи на сторінку
document.write(`<div class='main-page'>
    <header class="header">
        <h1>Домашня робота №1</h1>
    </header>
    <main class="main">
        <nav class="main-nav">
            <div class="tasks">
                
                <div class="tasks-1">
                    <h2>Картка користувача</h2>
                    <p>
                        Ім'я: ${userName}<br>
                        Призвище: ${userSurname} <br>
                        Вік: ${userAge}<br>
                    </p>
                    <p>
                        Перше число: ${number1}<br>
                        Друге число: ${number2}<br>
                        Третє число: ${number3}<br>
                        Середнє арифметичне <br>
                        цих чисел: ${resultAverageNumber}<br>
                    </p>
                </div>
            </div>
        </nav>
        <section class="main-section">
            <div class="tasks">
                <h2>Домашнє завдання №1</h2>
                <div class="tasks-1">
                    <h3>Завдання №1</h3>
                    <p>  
                        Умови:<br>                     
                        x = ${x = 6}<br>
                        y = ${y = 14}<br>
                        z = ${z = 4}<br>
                        Операція: x += y - x ++ * z <br>
                        Результат:<span class="green"> 6 += 14 - 6 * 4 = </span> ${x += y - x ++ * z} <br>
                        x = ${x}<br>
                        y = ${y}<br>
                        z = ${z}<br>
                    </p>
                    <p>
                        Приорітетність операторів:<br>
                        1. <span class="red">(6++ = 7) цей результат буде видно праворуч від х,тому він не враховуеться в данному прикладі</span><br>
                        2. <span class="green">(6 * 4 = 24)</span><br>
                        3. <span class="green">(14 - 24 = -10)</span><br>
                        4. <span class="green">(6 += -10 = -4)</span><br>
                    </p>
                </div>
                <div class="tasks-1">
                    <h3>Завдання №2</h3>
                    <p>  
                        Умови:<br>                     
                        x = ${x = 6}<br>
                        y = ${y = 14}<br>
                        z = ${z = 4}<br>
                        Операція: z = --x - y * 5 <br>
                        Результат: <span class="green"> 4 = --6 - 14 * 5 = </span>${z = --x - y * 5}<br>
                        x = ${x}<br>
                        y = ${y}<br>
                        z = ${z}<br>
                    </p>
                    <p>
                        Приорітетність операторів:<br>
                        1. <span class="green">(--6 = 5) Оператор (--) стоїть ліворуч від x а звернення до нього праворуч,тому результат враховується в цьому прикладі</span><br>
                        2. <span class="green">(14 * 5 = 70)</span><br>
                        3. <span class="green">(5 - 70 = -65)</span><br>
                        4. <span class="green">(z = -65)</span><br>
                    </p>
                </div>
                <div class="tasks-1">
                    <h3>Завдання №3</h3>
                    <p>  
                        Умови:<br>                     
                        x = ${x = 6}<br>
                        y = ${y = 14}<br>
                        z = ${z = 4}<br>
                        Операція: y /= x + 5 % z <br>
                        Результат:<span class="green"> 14 /= 6 + 5 % 4 = </span> ${y /= x + 5 % z}<br>
                        x = ${x}<br>
                        y = ${y}<br>
                        z = ${z}<br>
                    </p>
                    <p>
                    Приорітетність операторів:<br>
                        1. <span class="green">(5 % 4 = 1) <br></span>
                        2. <span class="green">(6 + 1 = 7)<br></span>
                        3. <span class="green">(14 /= 7 = 2)<br></span>
                    </p>
                </div>
                <div class="tasks-1">
                    <h3>Завдання №4</h3>
                    <p>  
                        Умови:<br>                     
                        x = ${x = 6}<br>
                        y = ${y = 14}<br>
                        z = ${z = 4}<br>
                        Операція: z - x++ + y * 5 <br>
                        Результат: <span class="green"> 4 - 6++ + 14 * 5 = </span>${z - x++ + y * 5}<br>
                        x = ${x}<br>
                        y = ${y}<br>
                        z = ${z}<br>
                    </p>
                    <p>
                    Приорітетність операторів:<br>
                        1. <span class="red">(6++ = 7) цей результат буде видно праворуч від х,тому він не враховуеться в данному прикладі</span><br>
                        2. <span class="green">(14 * 5 = 70)</span><br>
                        3. <span class="green">(4 - 6 = -2)</span><br>
                        4. <span class="green">(-2 + 70 = 68)</span><br>
                    </p>
                </div>
                <div class="tasks-1">
                    <h3>Завдання №5</h3>
                    <p>  
                        Умови:<br>                     
                        x = ${x = 6}<br>
                        y = ${y = 14}<br>
                        z = ${z = 4}<br>
                        Операція: x = y - x++ * z <br>
                        Результат: <span class="green"> 6 = 14 - 6++ * 4 = </span>${x = y - x++ * z}<br>
                        x = ${x}<br>
                        y = ${y}<br>
                        z = ${z}<br>
                    </p>
                    <p>
                    Приорітетність операторів:<br>
                        1. <span class="red">(6++ = 7) цей результат буде видно праворуч від х,тому він не враховуеться в данному прикладі</span><br>
                        2. <span class="green">(6 * 4 = 24)</span><br>
                        3. <span class="green">(14 - 24)</span><br>
                        4. <span class="green">(x = -10)</span><br>
                    </p>
                </div>
            </div>
        </section>
    </main>
    <footer class="footer">
        <p>Влад Бродич 22 Листопада 2022 рік.</p>
    </footer>
</div>`);