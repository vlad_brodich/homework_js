'use strict'// Суворий режим.
import { getElemId,rotateSlides} from "./functions.js";

// Створення тестових користувачів.

if(!localStorage.users){
  localStorage.setItem('users', JSON.stringify([{name:'Влад', birth:'1976-07-10',
  parents_name:'',
  mobile_number:'+3 8067 760 65 08',
  email:'brodich_vlad@ukr.net',
  home_number:'044 515 74 80',
  password: 'vlad123',
  data: '2023.02.28.11:19',}]))
};

if(!sessionStorage.isUserName || sessionStorage.isUserName === ''){
  sessionStorage.isUserName = '';
  document.location = '/authorization_page';
};



// Початок роботи після завантаження сторінки.
window.addEventListener('load',()=>{

  // Функція згортання випадаючого меню. 
  window.addEventListener ('click',(event) => {
    if (event.target !== getElemId("btnShowMenu")) {
      getElemId("myDropdown").classList.remove("show");
    };
  });

  // Кнопка випадаючого меню. 
  getElemId("btnShowMenu").addEventListener ('click', () => {getElemId("myDropdown").classList.add("show")});


  // Виклик функції "слайдер"
  window.setInterval(function(){rotateSlides()}, 5000);

  // Вивід данних користувача на сторінку.
  const users =  JSON.parse(localStorage.users)
  const user = JSON.parse(sessionStorage.isUserName)
  users.find(el => {
      if(el.email === user){
        getElemId('work-2').innerHTML = `Ім'я: <span>${el.name||'Данні відсутні'}</span><br>
        Пошта: <span>${el.email ||''}</span><br>
        Дата народження: <span>${el.birth||'Данні відсутні'}</span><br>
        Ім'я одного з батьків: <span>${el.parents_name||'Данні відсутні'}</span><br>
        Мобільний телефон: <span>${el.mobile_number||'Данні відсутні'}</span><br>
        Домашній телефон: <span>${el.home_number ||'Данні відсутні'}</span><br>
        Пароль: <span>${el.password ||'Данні відсутні'}</span><br>
        Дата реєстрації: <span>${el.data ||'Данні відсутні'}</span><br>`
      }
      else {

      }
  });
  getElemId('out').addEventListener ('click',(ev) => {
    sessionStorage.isUserName = '';
    document.location = '/';
  })
});


