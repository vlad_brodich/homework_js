'use strict'// Суворий режим.
import {createInput,createImg,createBtn} from "./functions.js";


// Патерни для перевірки.
export const paternName = /^[a-za-яієґї'-]{2,}$/ig,
paternEmail =  /^[a-z_.0-9]+@[a-z0-9-.]+\.[a-z.]+$/ig,
paternPassword = /^[A-z0-9]{6,}$/ig,
paternBirth = /[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/,
paternPhone = /^\+3\ 8\ 0\d{2}\ \d{3}\ \d{2}\ \d{2}$/,
paternHomePhone = /^0\d{2}\ \d{3}\ \d{2}\ \d{2}$/

// Оголошення змінної сторінки авторизації.
export const authorizationPage = `<div class="main-page">
<div class="authorization-page">
    ${createImg ("authorization-page__img","../imege/imege_homework/authorization_page-img.png","laptop")}
    <div class="authorization-page__form">
        <h2>Login</h2>
        <form id="authorization-form">
            <ul>
                ${createInput("email","email-authorization",'Email',"user@ukr.net")}
                ${createInput("password","password-authorization",'Password',"Password")}
            </ul>
        </form>
        <p>
            Not a user? <a href="../registration_page/index.html">Register now</a>
        </p>
        ${createBtn ("btn-login","btn-login",'Login')}
    </div>
</div>
</div> `;

// Оголошення змінної сторінки реєстрації.
export const registrationPage = `<div class="main-page">
<div class="registration-page">
    ${createImg ("registration-page__img","../imege/imege_homework/registration-page-img.png","figure")}
    <div class="registration-page__form">
        <h2>Registration form</h2>
        <form id="registration-form">
            <ul>
                ${createInput("text","user-name",'* Name',"Name")}
                ${createInput("date","user-birth",'* Date Of Birth',"")}
                ${createInput("text","user-parents-name",'Father’s/Mother’s Name',"Name")}
                ${createInput("email","email-registration",'* Email',"user@ukr.net")}
                ${createInput("tel","mobile-number",'* Mobile No.',"+3 8 067 222 11 33")}
                ${createInput("password","password-registration",'* Password',"Password")}
                ${createInput("password","enter-password-registration",'* Re-enter Password',"Re-enter Password")}
                ${createInput("tel","home-number",'home Number',"044 555 77 22")}
            </ul>
        </form>
        <p>* required fields</p>
        ${createBtn ("btn-submit","btn-submit",'Submit')}
    </div>
</div>
</div> `;
