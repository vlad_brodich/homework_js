'use strict'// Суворий режим.
// import {passwordUser} from "functions.js"
// Функція пошуку елемента по id.
export const getElemId= id => document.getElementById(id);

// // Оголошення змінних "слайдера".
// let slideIndex = 0;
// const slides = document.getElementsByClassName("slide");

// // Функція "слайдер" змінює картинки кожні 5 секунд.
// export const showSlides = () => {
//   for (let i = 0; i < slides.length; i++) {
//       slides[i].classList.remove('show-slide');  
//   }
//   slideIndex++;
//   if (slideIndex === slides.length) {
//       slideIndex = 0;
//   } 
//   slides[slideIndex].classList.add('show-slide');
//   // Таймер "слайдера".
//   setTimeout(showSlides, 5000); 
// };

//Функція "Новий-слайдер" на шість фото.
export function rotateSlides() {
  // Знаходимо слайд.
  const slider = getElemId('slider');
  // Знаходимо дочірній вузол слайду.
  const elemLastChild = slider.lastElementChild;
  // Клонування дочірнього вузла разом з вмістом.
  const lastChild = elemLastChild.cloneNode(true);
  // Всі слайди в колекції прибираємо клас 'firstSlide'.
  const [...sliders] = document.querySelectorAll('#slider li');
  sliders.forEach(el=>{el.classList.remove('firstSlide');});
  // Видаляємо дочірній елемент з попередньої позиції.
  elemLastChild.remove(elemLastChild);
  // Додаємо клон на нову позицію.
  slider.prepend(lastChild);
  // Додаємо клас 'firstSlide'.
  lastChild.classList.add('firstSlide')
}


// Функції домашньої роботи.


// Функція створення інпутів.
export function createInput (type,id,text,placeholder){
  return `<li>
  <label for="${id}">${text}</label>
  <input type="${type}" id="${id}" placeholder="${placeholder}">
</li>`
}

// Функція створення зображеннь.
export function createImg (clas,src,alt){
  return `<div class=";${clas}">
  <img src="${src}" alt="${alt}">
  </div>`
}

// Функція створення кнопки.
export function createBtn (clas,id,text){
  return `<button class="${clas}" type="button" id="${id}" disabled>${text}</button>`
}

// Функція створює поточну дату та час.
export const getDate = () =>{
  const date = new Date();
  let day =  date.getDate() > 9||`0${date.getDate()}`
  let month = date.getMonth()+1 > 9||`0${date.getMonth()+1}`
  const dataNew = [date.getFullYear(),month,day,`${date.getHours()}:${date.getMinutes()}`]
  return dataNew
}

// Функція очищення форми.
export function cleanElementsForm(elements) {
  elements.forEach(el =>{
      el.value = ''
      el.classList.remove("input-error");
  });
};

// Функція виводу вікна помилки.
export function errorWindow (text){
  const el = document.createElement('div');
  el.className = "error-box";
  el.innerHTML = `<p>${text}</p>`
  document.body.prepend(el);
  const remove =()=>{el.remove(el)};
  setInterval(remove,4000);
}

// Функція перевірки данних користувача для реєстрації.
export function userDataValid (user){
  const users =  JSON.parse(localStorage.users);
  let i = false;
  let a = 1;
  if(users.length === 0){
    document.location = '/registration_page'
  }
  else {
    users.find(el => {
    if(user.login === el.email){
      if(user.password === el.password){
        sessionStorage.isUserName = JSON.stringify(el.email)
        document.location = '/'
      }
      else {
        errorWindow (`Забули пароль? Пароль ${el.password}`)
        const location =()=>{document.location = '/authorization_page'};
        setInterval(location,4000);
      }
      i = true  
    }
    else if(i === false && a === users.length){
      errorWindow (`Користувача з такою поштою ${el.email} немає. Зареєструйтесь будь-ласка`)
      const location =()=>{document.location = '/registration_page'};
      setInterval(location,4000);
      console.log(el.email)
    }
    a +=1;
  });
  }
};

// Функція тест валідація.
export const validate = (p, v) => p.test(v);
