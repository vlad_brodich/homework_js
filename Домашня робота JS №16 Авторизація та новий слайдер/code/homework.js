﻿'use strict'// Суворий режим.

import {getElemId,validate,getDate,userDataValid,cleanElementsForm,errorWindow} from "./functions.js";


import {authorizationPage,registrationPage,paternName,paternEmail,paternPassword,paternBirth,paternPhone,paternHomePhone} from "./methods.js";


// Логін пароль тестового користувача.
console.log('Логін тест:  brodich_vlad@ukr.net')
console.log('Пароль тест:  vlad123')
export const passwordUser = (pass) =>{
    console.log(pass)
};

// Як-що сторінка авторизації.
if(document.location.pathname.includes("/authorization_page/")){

    // Вивід розмітки на сторінку.
    document.body.insertAdjacentHTML("afterbegin",authorizationPage);
    sessionStorage.isUserName = '';
    // Створення змінноі користувач.
    const userData = {
        login:'',
        password: '',
    },

    // Пошук кнопки btnLogin.
    btnLogin = getElemId('btn-login');
    btnLogin.addEventListener('click',()=>{
        userDataValid (userData)
        cleanElementsForm(elementsForm)
    });

    // Пошук інпутів та додавання події 'change'.
    const [...elementsForm] = document.querySelectorAll('#authorization-form input');

    elementsForm.forEach(el =>{
        el.addEventListener('change',(el) =>{
            inputChangeEvent(el.target)
        });
    });

  
    // Функція перевірки інпутів на яких відбулась подія.
    function inputChangeEvent(event){
        if(event.id === "email-authorization" && validate(paternEmail, event.value)){
            event.classList.remove("input-error");
            userData.login = event.value;
        }
        else if(event.id === "password-authorization" && validate(paternPassword, event.value)){
            event.classList.remove("input-error")
            userData.password = event.value;
            if(userData.login !== ''){
                btnLogin.disabled = false
            }
        }
        else{
            event.classList.add("input-error")
            return
        }
    };
   
}// Як-що сторінка реєстрації.
else if(document.location.pathname.includes("/registration_page/")){

    // Вивід розмітки на сторінку.
    document.body.insertAdjacentHTML("afterbegin",registrationPage);
    sessionStorage.isUserName = '';
    // Створення змінноі користувач.
    const newUserData = {
        name:'',
        birth:'',
        parents_name:'',
        mobile_number:'',
        email:'',
        home_number:'',
        password: '',
        data: '',
    },
    users =  JSON.parse(localStorage.users),
 
    // Пошук кнопки btnSubmit та додавання події 'click'.
    btnSubmit = getElemId('btn-submit');
    btnSubmit.addEventListener('click',()=>{
        usersNewUser (newUserData)
        cleanElementsForm(elementsForm)
        sessionStorage.isUserName = JSON.stringify(newUserData.email)
        document.location = '/'
    });

    // Пошук інпутів додавання події 'change'.
    const [...elementsForm] = document.querySelectorAll('#registration-form input');
    elementsForm.forEach(el =>{
        el.addEventListener('change',(el) =>{
            inputRegisChangeEvent(el.target)
        });
    });

    // Функція додавання користувача в локальну память.
    function usersNewUser(newUserData){
    newUserData.data = getDate ().join('.')
    users.push(newUserData);
    localStorage.users = JSON.stringify(users);
    }
    // Перевіряє чи є користувач з такою поштою.
    const userEmail = (el) =>{
        if(users.find(item => item.email === el.value)){
            errorWindow (`Користувач з такою ${el.value} адресою вже зареєстрований.`)
            el.value = '';
            return false
        }
        else {
            el.classList.remove("input-error");
            return true
        }
    }
    // Створення змінної тимчасовий пароль.
    let temporaryPassword ='';
    // Функція перевірки данних отриманих з інпутів.
    function inputRegisChangeEvent(event){

        if(event.id === "user-name" && validate(paternName, event.value)){
            event.classList.remove("input-error");
            newUserData.name = event.value
        }
        else if(event.id === "user-birth" && validate(paternBirth, event.value)){
            event.classList.remove("input-error");
            let ar = event.value.split('-');
            if(getDate ()[0]-ar[0]*1 <= 18 || 80 < getDate ()[0]-ar[0]*1 ){
                event.classList.add("input-error")
            }
            else {
                event.classList.remove("input-error");
                newUserData.birth = event.value
            }
        }
        else if(event.id === "user-parents-name" && validate(paternName, event.value)){
            event.classList.remove("input-error");
            newUserData.parents_name = event.value
        }
        else if(event.id === "email-registration" && validate(paternEmail, event.value) && userEmail(event)){
            event.classList.remove("input-error");
            newUserData.email = event.value;
        }
        else if(event.id === "mobile-number" && validate(paternPhone, event.value)){
            event.classList.remove("input-error");
            newUserData.mobile_number = event.value;
        }
        else if(event.id === "password-registration" && validate(paternPassword, event.value)){
            event.classList.remove("input-error")
            temporaryPassword = event.value;
        }
        else if(event.id === "enter-password-registration" && event.value === temporaryPassword){
            event.classList.remove("input-error");
            newUserData.password = event.value;
        }
        else if(event.id === "home-number" && validate(paternHomePhone, event.value)){
            event.classList.remove("input-error");
            newUserData.home_number = event.value;
        }
        else{
            event.classList.add("input-error")
            return
        }

        if(newUserData.password !== '' && newUserData.email !== '' && newUserData.name !== '' && newUserData.birth !== '' && newUserData.mobile_number !== ''){
            btnSubmit.disabled = false
        }
    };
};