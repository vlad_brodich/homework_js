'use strict'// Суворий режим
import {get,getServer} from "./functions.js";

// Функція згортання випадаючого меню. 
window.addEventListener ('click',(event) => {
  if (event.target !== get("btnShowMenu")) {
    get("myDropdown").classList.remove("show");
  };
});

// Кнопка випадаючого меню. 
get("btnShowMenu").addEventListener ('click', () => {get("myDropdown").classList.add("show")});

/* Виконати запит на https://swapi.dev/api/people (https://starwars-visualguide.com) отримати список героїв зіркових воєн.
Вивести кожного героя окремою карткою із зазначенням.
Картинки Імені, статевої приналежності, ріст, колір шкіри,рік народження та планету на якій народився.Створити кнопку зберегти на кожній картці.
При натисканні кнопки записуйте інформацію у браузері. */


// Знайти та показати loader.
const loader = document.querySelector(".box-loader");
loader.classList.add("show-loader");

// Масиви отримані та збережені данні.
const heroes = [],
userHeroes = [],

// Функція створення кнопок "Зберегти данні".
button = (id)=>{
  const el = document.createElement('button');
  el.type = 'button';
  el.textContent = 'Зберегти данні?';
  el.dataset.id = id;
  el.addEventListener('click',()=>{
    buttonClick(el.dataset.id)
  });
  return el;
},

// Функція запису данних в пам'ять браузера після кліку на кнопку "Зберегти данні".
buttonClick = (id) =>{
  heroes.forEach(el =>{
    if(el.name === id){
      userHeroes.push(el);
    }
  });
  // Збереження данних в пам'ять браузера.
  // localStorage.setItem('user_heroes', JSON.stringify(userHeroes));
  sessionStorage.setItem('user_heroes', JSON.stringify(userHeroes));
};

// Кнопка перегляду збережених данних.
get('btn-user-heroes').addEventListener('click',()=>{showUserHeroes()});

// Функція виводу збережених данних.
function showUserHeroes(){
    // Отримання збережених данних.
  // const restoredUserHeroes = JSON.parse(localStorage.getItem('user_heroes'));
  const restoredUserHeroes = JSON.parse(sessionStorage.getItem('user_heroes'));
  // Очищення сторінки.
  get('user-heroes').innerHTML = '';
 
  if(restoredUserHeroes){
    restoredUserHeroes.forEach(el =>{
      get('user-heroes').insertAdjacentHTML("beforeend",`<li>${el.img}<div><p>Ім'я: <span>${el.name}</span></p>
      <p>Стать: <span>${el.gender}</span></p>
      <p>Зріст: <span>${el.height}</span></p>
      <p>Колір шкіри: <span>${el.color}</span></p>
      <p>Рік народження: <span>${el.year}</span></p>
      <p>Рідна планета: <span>${el.homewor}</span></p></div>
      </li>`)
    });
  }
  else{
    get('user-heroes').innerHTML = `<li><p>Немає збережених карток</p>
    </li>`
  };
  // Очищення пам'яті.
  // localStorage.removeItem('user_heroes');
  // localStorage.clear();
};

// Функція виводу отриманих данних на сторінку.
function showHeroes(data = []) {
  data.results.forEach((el,i)=> {
    const ajax = new XMLHttpRequest();
    ajax.open("get", el.homeworld);
    ajax.send();
    ajax.addEventListener("readystatechange", () => {
      if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
        let a = JSON.parse(ajax.response).name;
        let li = document.createElement('li');
        li.innerHTML = `<img src="https://starwars-visualguide.com/assets/img/characters/${i+1}.jpg" alt="${el.name}">
        <div><p>Ім'я: <span>${el.name}</span></p>
        <p>Стать: <span>${el.gender}</span></p>
        <p>Зріст: <span>${el.height}</span></p>
        <p>Колір шкіри: <span>${el.skin_color}</span></p>
        <p>Рік народження: <span>${el.birth_year}</span></p>
        <p>Рідна планета: <span>${a}</span></p></div>`
        li.appendChild(button(el.name));
        get('heroes').append(li);
        heroes.push({
          img: `<img src="https://starwars-visualguide.com/assets/img/characters/${i+1}.jpg" alt="${el.name}"></img>`,
          name: el.name,
          gender : el.gender,
          height : el.height,
          color : el.height,
          year : el.birth_year,
          homewor: a,
        });
        if(i+1 ===  data.results.length){loader.classList.remove("show-loader")};
      } 
      else if (ajax.readyState === 4) {
        let li = document.createElement('li');
        li.innerHTML = `<img src="https://starwars-visualguide.com/assets/img/characters/${i+1}.jpg" alt="${el.name}">
        <div><p>Ім'я: <span>${el.name}</span></p>
        <p>Стать: <span>${el.gender}</span></p>
        <p>Зріст: <span>${el.height}</span></p>
        <p>Колір шкіри: <span>${el.skin_color}</span></p>
        <p>Рік народження: <span>${el.birth_year}</span></p>
        <p>Рідна планета: <span>Планета не відома</span></p></div>`
        li.appendChild(button(el.name));
        get('heroes').append(li);
        heroes.push({
          img: `<img src="https://starwars-visualguide.com/assets/img/characters/${i+1}.jpg" alt="${el.name}"></img>`,
          name: el.name,
          gender : el.gender,
          height : el.height,
          color : el.height,
          year : el.birth_year,
          homewor: 'Планета не відома',
        });
        if(i+1 ===  data.results.length){loader.classList.remove("show-loader")};
        };
      });
    });
};

// Виклик функції запиту на сервер.
getServer('https://swapi.dev/api/people',showHeroes);