'use strict'// Суворий режим
import {get,getServer,elConstr} from "./functions.js";

// Функція згортання випадаючого меню. 
window.addEventListener ('click',(event) => {
  if (event.target !== get("btnShowMenu")) {
    get("myDropdown").classList.remove("show");
  };
});

// Кнопка випадаючого меню. 
get("btnShowMenu").addEventListener ('click', () => {get("myDropdown").classList.add("show")});

// Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
// Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
// На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
// сторінок показувати нові коментарі. 
// з коментарів виводити : 
// "id": 1,
// "name"
// "email"
// "body":

// Знайти та показати loader.
const loader = document.querySelector(".box-loader");
loader.classList.add("show-loader");
// Масив для упарядкованих коментарів.
const coments = [],

// Отримання та упарядкування коментарів.
mainComents = (data = [])=>{
  let i = 0;
  data.forEach(el => {
    if(el.id === 1){
      coments.push([el])
    }
    else if(el.id % 10 === 0 && i < data.length/10-1){
        coments.push([])
        coments[i].push(el)
        i += 1;
      }else {
        coments[i].push(el)
      }
  });
  showComents(0);
  loader.classList.remove("show-loader");
};
// Запит на сервер.
getServer('https://jsonplaceholder.typicode.com/comments',mainComents)

// Відображення поля пагінації.
let i = 1;
const showList = (id)=>{
  if(id === "right"){
    get(`list-${i}`).classList.remove('list-show')
    i += 1
    get(`list-${i}`).classList.add('list-show')
  }
  else if(id === "left"){
    get(`list-${i}`).classList.remove('list-show')
    i -= 1
    get(`list-${i}`).classList.add('list-show')
  }
  if(i === 1){
    get('left').disabled = true;
  }
  else {
    get('left').disabled = false;
  }
  if(i === coments.length/10){
    get('right').disabled = true;
  }
  else {
    get('right').disabled = false;
  }
};

// Слухач події для поля навігації.
get('nav-js').addEventListener ('click',(ev) =>{
  const [...el] = document.querySelectorAll('.page-link');
  el.forEach(el =>{
    el.classList.remove("active");
  })

  if(ev.target.id === "left" && i !==1){
    showList(ev.target.id)
  }
  else  if(coments.length === 0){
    get('right').disabled = true;
    return;
  }
  else if(ev.target.id === "right" && i !== coments.length/10){
    showList(ev.target.id)
  }
  else if(ev.target.dataset.id){
    ev.target.classList.add("active")
    showComents(ev.target.dataset.id)
  }
});

// Вивід коментарів на сторінку.
function showComents(a){
 let comenList = coments[a];
  //  Отримання дати.
 let data = `${new Date().getDate()}-${new Date().getMonth() < 10 ? `0${new Date().getMonth() + 1}` : new Date().getMonth() + 1}-${new Date().getFullYear()} р.`;
  //  Очищення сторінки.
 get('coment-list').innerHTML = '';
 comenList.forEach(el =>{
    // Вивід коментарів на сторінку.
    get('coment-list').insertAdjacentHTML("beforeend",`<li>
      <p>Id: <span>${el.id}</span></p>
      <p>Ім'я: <span>${el.name}</span></p>
      <a class=" link-focus" href="mailto:${el.email}">Пошта: ${el.email}</a>
      <p>Коментар: <span>${el.body}</span></p>
      <p>Дата: <span>${data}</span></p>
      </li>`);
  });
  //  Вивід номеру сторінки.
  get('title').innerText = `Сторінка коментарів №${a*1 + 1}.`;
  showNav()
};

// Функція створення поля навігації в залежності від кількості сторінок.

let a = 1,b = 0,c = 0;

function showNav(){
  for (a; a <= coments.length/10; a++) {
    let el = elConstr('ul','nav-list')
    el.id = `list-${a}`
    if(a===1){
      el.classList.add("list-show")
    }
    get('nav-list-js').appendChild(el)
    for (b; b < 10; b++) {
      if(c === 0){
        el.insertAdjacentHTML("beforeend",`<li class="page-item">
        <a data-id="${c}" class="page-link active" href="#main-page">${c+1}</a>
    </li>`);
      }
      else if(c !== coments.length/10){
        el.insertAdjacentHTML("beforeend",`<li class="page-item">
        <a data-id="${c}" class="page-link" href="#main-page">${c+1}</a>
    </li>`);
      }
      c += 1
    } 
    b = 0;
  }
};

