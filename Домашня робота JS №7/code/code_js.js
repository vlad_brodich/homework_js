'use strict'// Суворий режим

// Оголошення функцій

// Функція для випадаючого меню 
function showMenu() {
    document.getElementById("myDropdown").classList.toggle("show");
};
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    let dropdowns = document.getElementsByClassName("dropdown-content");
    for (let i = 0; i < dropdowns.length; i++) {
      let openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      };
    };
  };
};
document.getElementById("btnShowMenu").onclick = (e) => {
  showMenu();
};
// Функція виводу данних на сторінку selector = сclass="Куди потрібно вивести",text = Текст який потрібно вивести.position - "afterBegin" перед попереднім   "beforeEnd"  після попереднього
function showResult(selector,text = `<p>Результат виконання: <span class="red">Не виконано</span></p>`,position = "beforeEnd"){
    document.querySelector(selector).insertAdjacentHTML(`${position}`,
    text);
};

// Домашня робота №7

/*Завдання №1

- Написати функцію `filterBy()`, яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані,
другий аргумент - тип даних.
- Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип
яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом
передати 'string', то функція поверне масив [23, null].  String  Number*/

const arrType = [{  }, Symbol("id"), function(  ){ }, 10n,[ ], , undefined, false, null, true, 'hello', 'world', 45, 77, '23'];

/*Результатом typeof null є "object". Це офіційно визнана помилка поведінки typeof, що є ще з ранніх днів JavaScript і зберігається для сумісності. Безперечно, null не є об’єктом. Це особливе значення з власним типом. У цьому разі поведінка typeof некоректна. Тому додана додаткова перевірка*/

function filterBy(arr,arg){
  if(arg === "null"){
    return arr.filter(item => item !== null);
  }
  else if(arg === "object"){
    let newArr =[];
    arr.forEach(function(item) {
      if(typeof item === 'object' && item === null){
        newArr.push(item);
      }
      else if(typeof item !== 'object'){
        newArr.push(item);
      }
    });
    return newArr;
  }
  else return arr.filter(item => arg !== typeof item);
};
  // Ця функція потрібна для коректного відображення елементів масиву на HTML сторінці
function strFilterBy(arr,arg){
  let resultArr = filterBy(arr,arg);
  let str = '';
  resultArr.forEach(function(item, index){
    if(typeof item === 'symbol'){
      item = 'Symbol("id")';
    }
    else if(item === null){
      item = 'null';
    }
    else if(typeof item === "undefined"){
      item = 'undefined';
    }
    else if(typeof item === "object"){
      item = 'object';
    }
    else if(typeof item === "bigint"){
      item = '10n';
    }
    str += ` ${item}, `;
  });
  return str;
};

// Функція перевіряє яку позицію обрав користувач зі списку
function validEmployees(){
  let el = document.getElementById('el_pr');
  for(let i = 0; i < el.options.length; i ++){
    if(el.options[i].selected === true) {
      return el.options[i].value;
    } 
  };
};

// Функція вивід інформації на сторінку
function showFilterBy(){
  showResult('.js-box-1', `<p>Результат вилучення типу даних: "${validEmployees()}" з масиву. <br><span class="green">[${strFilterBy(arrType,validEmployees())}]</span><br><span class="red">Докладніше результат виконаня можна побачити в консолі розробника</span></p>`,"afterBegin");
};
document.getElementById("btnShowFilterBy").onclick = (e) => {
  showFilterBy();
};
// Вивід інформації в консоль
console.log(arrType); // Оригінальний масив
console.log(filterBy(arrType,"string")); // Масив після вилучення типу даних "string"
console.log(filterBy(arrType,"number")); // Масив після вилучення типу даних "number"
console.log(filterBy(arrType,"object")); // Масив після вилучення типу "object"
console.log(filterBy(arrType,"undefined"));// Масив після вилучення типу даних "undefined"
console.log(filterBy(arrType,"boolean"));// Масив після вилучення типу даних "boolean"
console.log(filterBy(arrType,"bigint"));// Масив після вилучення типу даних "bigint"
console.log(filterBy(arrType,"function"));// Масив після вилучення типу "function"
console.log(filterBy(arrType,"symbol"));// Масив після вилучення типу даних "symbol"
console.log(filterBy(arrType,"null"));// Масив після вилучення типу даних "null"

// Завдання №2 Переписати гру 'шибениця' з книги на новий синтасис.

/* #1. Більше слів
Додати нові слова в масив words.
#2. Заголовні букви
Якщо гравець введе заголовну літеру, вона не збігається з такою самою
малою літерою в загаданому слові.
#3. Обмеження по ходам
 Додайте до програми змінну для обліку спроб і завершуйте гру, якщо гравець
витратив усі спроби.
#4. Виправте помилку
У грі є помилка: якщо ви будете знову і знову вводити одну і ту
ж літеру, яка є в загаданому слові, remainingLetters знову і знову зменшуватиметься. Намагайтеся це виправити!*/

// Створюємо масиви зі словами.

// Тема: Міста України.
const wordsСity = ["Волноваха","Гостомель","Маріуполь","Харків","Херсон","Чернігів","Буча","Ірпінь","Миколаїв","Охтирка",];
// Тема: Типи даних в JS.
const wordsJs = ["string","number","object","undefined","boolean","symbol","null",];
// Тема: HTML теги.
const wordsHtml = ["Div","body","header","section","form","footer","button","main",];

// Обираємо тему гри
function validIteam(){
  let el = document.getElementById('el_pr__work-2');
  for(let i = 0; i < el.options.length; i ++){
    if(el.options[i].selected === true) {
      if(el.options[i].value === 'city'){
        return wordsСity;
      }
      else if(el.options[i].value === 'js'){
        return wordsJs;
      }
      else {
        return wordsHtml;
      };
    };
  };
};

// Обираємо випадкове слово
function wordRandom(){
  let words = validIteam();
  return words[Math.floor(Math.random() * words.length)].toLocaleUpperCase()
};

// Функція керування грою
function game (){
  // Визначаємо мову
  let langType = "";
  if(document.getElementById('el_pr__work-2').value === 'city'){
    langType = 'Українську';
  }
  else {
    langType = 'Ангійську';
  };
  let answerArray = [];
  let word = wordRandom();
  for (let i = 0; i < word.length; i++) {
    answerArray[i] = "_";
  };
  let remainingLetters = word.length;
  let samples = word.length + 3;
  // Результат гри
  let result = "Чудово! Було загадане слово"
  // Ігровий цикл
  while (remainingLetters > 0) {
    //Показуємо стан гри
    alert(answerArray.join(" "));
    // Запитуєм варіант відповіді
    let guess = prompt(`Вгадайте загадане слово з ${word.length} літер. Введіть ${langType} літеру, Кількість спроб що залишилось (${samples}). Або натисніть "Скасувати" для завершення гри.`);
    if (guess === null) {
    // Завершуєм гру
    result ='До зустрічи. Було загадане слово';
    break;
    } 
    else if (guess.length !== 1) {
    alert(`Будь ласка введіть одну ${langType} літеру`);
    } 
    else {
      // Оновлюємо стан гри
      guess = guess.toLocaleUpperCase();
      for (let j = 0; j < word.length; j++) {
        if (word[j] === guess && answerArray[j] === '_') {
          answerArray[j] = guess;
          remainingLetters -- ;
        }
      }
    }
    samples -- ;
    if(samples === 0){
      result ='Кількість спроб вичерпана. Було загадане слово';
      break
    }
    //Кінець гри
  };
  // Виводемо відповідь та вітаємо ігрока
  alert(`${result} ${word}`);
};
document.getElementById("btnGame").onclick = (e) => {
  game();
};