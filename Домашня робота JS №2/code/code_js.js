// Оголошення змінних
let horizLine = 20;  // Розмір фігури по вертикалі
let vertLine = 15;    // Розмір фігури по горізонталі
const star = '*';       // Символ зірочка
const gap = '&nbsp;'// Символ пробіл
const transfer = "<br>"; //Символ переносу строки
let figure1 =''; // Заповнений прямокутник
let figure2 = ''; // Не заповнений прямокутник
let figure3 = ''; // Рівносторонній трикутник
let figure4 = ''; // Прямокутний трикутник
let figure5 = ''; // Ромб
let figure7 = ''; // Не заповнений Ромб
let figure6 = ''; // Ялинка
// Заповнений прямокутник
vertLine = 40;
horizLine = 20;
for(let i=0; i < horizLine; i++){
    for(let c=0;c < vertLine; c++){
        figure1 += (star);
    }
    figure1 += (transfer);
}
// Не заповнений прямокутник
vertLine = 30;
horizLine = 60;
for (let i = 0; i < vertLine; i++) {
    let figure = '';
    if (i === 0 || i === vertLine - 1) {    
      for (let j = 0; j < horizLine; j++) {
        figure += star;
      }   
      figure += transfer;
    } else {
        figure = star;
      for (let j = 0; j < horizLine - 2; j++) {
        figure += gap + gap;
      }    
      figure += star;
    figure += transfer;
    }   
    figure2 += figure;
}
// Рівносторонній трикутник
vertLine = 40;
for(let i = 0; i < vertLine; i++){
    for(let j = i + 1; j < vertLine; j++){
        figure3 += gap;
    }
    for(let y = 1+i; y > 0; y--){
        figure3 += star;
    }
    figure3 += transfer;
}
// Прямокутний трикутник
vertLine = 40;
for(let i = 0; i < vertLine; i++){
    for(let y = 1+i; y > 0; y--){
            figure4 += star;
        }
    figure4 += transfer;       
}
// ромб
horizLine = 30;
for(let i = 0; i < horizLine; i++){
    for(let j = i + 1; j < horizLine; j++){
        figure5 += gap;
    }
    for(let y = 1+i; y > 0; y--){
        figure5 += star;
    }
    figure5 += transfer;
}
for(let i = 0; i < horizLine; i++){
    for(let y = 1+i; y > 0; y--){
        figure5 += gap;
    }
    for(let j = i + 1; j < horizLine; j++){
        figure5 += star;
    }
    figure5 += transfer;
}
// Не заповнений Ромб
horizLine = 20;
for (let i = -horizLine; i <= horizLine; i++) {
  let figure = '';
  for (let j = -horizLine; j <= horizLine; j++) {
    if (Math.abs(i) + Math.abs(j) === horizLine) {  
      figure += star;
    } else {
      figure += gap;
    }
  }
  figure += transfer;
  figure7 += figure;
}
// Ялинка
for(let a = 5; a <= 15; a +=5){
    let figure = '';
    for(let i = 0; i < a; i++){
        for(let j = i + 1; j < 15; j++){
            figure += gap;
        }
        for(let y = 1+i; y > 0; y--){
            figure += star;
        }
        figure += transfer;
    }
    figure6 += figure;
}







// Вивід домашньої роботи на сторінку
document.write(`<div class='main-page'>
    <header class="header">
        <h1>Домашня робота №2</h1>
    </header>
    <main class="main">
        <section class="main-section">
            <div class="tasks">
                <h2>За допомогою циклу (for) намалювати геометричні фігури</h2>
                <div class="tasks-1">
                    <h3>Завдання №1 Заповнений прямокутник</h3>
                    <p class="tasks__text-1">             
                        ${figure1}
                    </p>
   
                </div>
                <div class="tasks-1">
                    <h3>Завдання №2 Не заповнений прямокутник</h3>
                    <p class="tasks__text-2">                     
                        ${figure2}
                    </p>
                </div>
                <div class="tasks-1">
                    <h3>Завдання №3 Рівносторонній трикутник</h3>
                    <p class="tasks__text-3">  
                        ${figure3}
                    </p>
                </div>
                <div class="tasks-1">
                    <h3>Завдання №4 Прямокутний трикутник</h3>
                    <p class="tasks__text-4">
                        ${figure4}  
                    </p>
                </div>
                <div class="tasks-1">
                    <h3>Завдання №5 Ромб</h3>
                    <p class="tasks__text-5">
                        ${figure5}  
                    </p>
                </div>
                <div class="tasks-1">
                <h3>Завдання №6 Не заповнений Ромб</h3>
                <p class="tasks__text-7">
                    ${figure7}  
                </p>
            </div>
                <div class="tasks-1">
                <h3>Завдання №7 Ялинка</h3>
                <p class="tasks__text-6">
                    ${figure6}  
                </p>
            </div>
            </div>
        </section>
    </main>
    <footer class="footer">
        <p>Влад Бродич 26 Листопада 2022 рік.</p>
    </footer>
</div>`);

