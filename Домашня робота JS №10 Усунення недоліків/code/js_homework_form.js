﻿'use strict'// Суворий режим

// Домашня робота №10

// Завдання 2
// Реалізуйте програму перевірки телефону
// * Використовуючи JS Створіть поле для введення телефону та кнопку збереження
// * Користувач повинен ввести номер телефону у форматі 000-000-00-00
// * Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний
// зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку
// https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
// якщо буде помилка, відобразіть її в діві до input.


// Севорення модального вікна з формою
document.getElementById('js-work-2').insertAdjacentHTML("beforeEnd",`<div class="modal" id="modal">
<form class="modal-form" id="modal-form">
<img class="close" id="modal-close-btn" src="./imege/Close.svg" alt="Close">
    <p id="result"></p>
    <div>
        <img src="./imege/phone.svg" alt="phone" width="25">
        <input class="input-tel" type="tel" id="tel-user" placeholder="000-000-00-00">
    </div>
    <button class="modal-form__btn" id="modal-btn" type="button">Зберегти</button>
</form>
</div>`);

// Функція перенаправлення користувача
const locationUser = () => {document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg'},

  // Функція закриття модального вікна
  removeModal = () => {
    get("modal").classList.toggle("show-modal");
    document.body.classList.toggle("stop-overflow");
  },

  // Основний контент форми
  startMessage = () => {
    classRemAdd('result','false','result');
    classRemAdd('tel-user','false','input-tel');
    classRemAdd('modal-form','false','modal-form');
    showMessage('result',`Введіть свій номер телефону`);
    get('tel-user').value = '';
  },

  // Повідомлення якщо форма порожня
  warningMessage = () => {
    classRemAdd('tel-user','input-tel-red','input-tel');
    classRemAdd('modal-form','modal-form-text-red','modal-form');
    showMessage('result',`Поле вводу не має бути порожнім!`);
    // Повенення до основної форми
    setTimeout(startMessage, 2000);
  },

  // Повідомлення про помилку
  errorMessage = () => {
    classRemAdd('modal-form','modal-form-red','modal-form');
    showMessage('result',`Введений номер телефону <br/>
    Не відповідає формату: 000-000-00-00<br/>Виправте будь-ласка.`,'html');
      // Повенення до основної форми
    setTimeout(startMessage, 2000);
  },

  // Повідомлення про успішне відправлення данних
  okMessage = () => {
    classRemAdd('result','false','result');
    showMessage('modal-form',`Вітаю<br/>Ваші данні успішно збережені`,'html');
    classRemAdd('modal-form','modal-form-green','modal-form');
  },

  // Функція перевірки отриманих данних
  input = (el) => {
    // Регулярний вираз
    // const pattern = /\d\d\d\-\d\d\d\-\d\d\-\d\d/; Не має перевірки на прбіли з початку та в кінці
    const pattern = /^\d\d\d\-\d\d\d\-\d\d\-\d\d$/;
    // Умови перевірки
    if(el === ''){
      warningMessage();
    }
    else if (pattern.test(el)) {
      okMessage();
      // Закриття модального вікна
      setTimeout(removeModal, 3000);
      // Перенаправлення користувача
      setTimeout(locationUser, 3500);
    }
    else {
      errorMessage();
    };
  },

  // Функція отримання данних від користувача.
  usserTel = () =>{
    let elUser = get('tel-user').value;
    input(elUser);
  };

// Функція кнопки закриття модального вікна.
get('modal-close-btn').onclick = () => {
  removeModal();
};

// Функція кнопки 'Відправити данні'.
get('modal-btn').onclick = () => {
  // Емітація обробки данних
  get("modal-form").classList.add("modal-filter");
  setTimeout(usserTel, 1000);
};

// Функція кнопки 'Перевірити завдання'.
get("js-work-2-btn").onclick = () => {
  startMessage();
  removeModal();
};
