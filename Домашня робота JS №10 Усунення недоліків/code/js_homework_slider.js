﻿'use strict'// Суворий режим


// Домашнє завдання №10 b

// Створіть слайдер кожні 3 сек змінюватиме зображення.
// Зображення для відображення
//  1. https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
//  2. https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
//  3. https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
//  4. https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg

// 5. https://i.photographers.ua/images/pictures/13829/2022_03_01_s_mi-z-ukraini.jpg
// 6. https://images.1plus1.video/news-1/41190/f00afca514a612d232cbada90df7584f.1020x565.jpg
// 7. https://vafk.com.ua/wp-content/uploads/2022/03/image_2022-03-01_09-47-20.png

// 8. https://pbs.twimg.com/media/FM4SyJyWYBEwD8j.jpg:large

// 9. https://ps.informator.ua/wp-content/uploads/2022/07/screenshot_1-6.png

let slideIndex = 0;

const slides = document.getElementsByClassName("slide"),

showSlides = () => {
    for (let i = 0; i < slides.length; i++) {
        slides[i].classList.remove('show-slide');  
    }
    slideIndex++;
    if (slideIndex === slides.length) {
        slideIndex = 0;
    } 
    slides[slideIndex].classList.add('show-slide');
    setTimeout(showSlides, 3000); 
};

showSlides();
