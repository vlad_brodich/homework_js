'use strict'// Суворий режим

// Оголошення функцій

// Функція пошуку елемента по id
const get = id => document.getElementById(id);

// Функція згортання випадаючого меню 
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    const [...dropdowns] = document.getElementsByClassName("dropdown-content");
    dropdowns.forEach((item)=>{
      if (item.classList.contains('show')){
        item.classList.remove('show');
      };
    });
  };
};

// Кнопка випадаючого меню 
get("btnShowMenu").onclick = () => {get("myDropdown").classList.toggle("show")};

