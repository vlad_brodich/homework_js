﻿'use strict'// Суворий режим

// Домашня робота №10

// Завдання 1
// Створіть програму секундомір.
// * Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
// * При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
// * Виведення лічильників у форматі ЧЧ:ММ:СС
// * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції

// Оголошення змінних таймера
let counterSecond = 0, counterMinut = 0, counterHour = 0, intervalHandler, flag = false,setInt = 1000,hour = 24,minut = 60,second = 60,milSecond = 100;

// Функція додає контент на сторінку
const showMessage = (argId,result,elType) =>{
    if(elType === 'html'){
      get(argId).innerHTML = result;
    }
    else get(argId).textContent = result;
  },

  // Функція зміни кольору таймера
  classRemAdd = (argId,argClass,keepClass) => {
    get(argId).className = keepClass;
    if(argClass !== 'false'){
      get(argId).classList.add(argClass);
    }
  },

 // Функція перемикання годинник - секундомір
  options = () => {
    if(get("opt1").checked){
      setInt = 1000;

      counterHour = 0;
      counterMinut  = 0;
      counterSecond = 0;

      showMessage("second",`00`);
      showMessage("minut",`00`);
      showMessage("hour",`00`);

      showMessage("info1",'Часи.');
      showMessage("info2",'Хвил.');
      showMessage("info3",'Секун.');
      
    }
    else if(get("opt2").checked){
      setInt = 10;
      counterHour = 0;
      counterMinut  = 0;
      counterSecond = 0;

      showMessage("second",`00`);
      showMessage("minut",`00`);
      showMessage("hour",`00`);

      showMessage("info1",'Хвил.');
      showMessage("info2",'Секун.');
      showMessage("info3",'Міл/Сек.');
      
    };
  },
    // Функція керування виводом значень на дісплей таймера &#32;&#32;&#32;&nbsp;
    count = () => {
      //Умови виводу маркерів
      if(setInt === 1000){
        if(counterSecond % 2 !== 0){
          showMessage('mark2','.');
        }
        else{showMessage('mark2',':');};
      }
      else if(setInt === 10){
        if(counterMinut % 2 !== 0){
          showMessage('mark1','.');
          showMessage('mark2','.');
        }
        else{
          showMessage('mark1',':');
          showMessage('mark2',':');
        };
      };
     
      //Умови виводу секунд
      if(counterSecond <= 9){
        showMessage("second",`0${counterSecond}`);
      }
      else if(counterSecond === milSecond){
        counterMinut ++
        counterSecond = 0;
        showMessage("second",`00`);
      }
      else {
        showMessage("second",`${counterSecond}`);
      }
  
      // Умови виводу хвилин
      if(counterMinut <= 9){
        showMessage("minut",`0${counterMinut}`);
      }
      else if(counterMinut === second){
        counterHour ++
        counterMinut = 0;
        showMessage("minut",`00`);
      }
      else {
        showMessage("minut",`${counterMinut}`);
      }
      // Умови виводу годин
      if(counterHour <= 9){
        showMessage("hour",`0${counterHour}`);
      }
      else if(counterHour === 60){
        counterHour = 0;
        showMessage("hour",`00`);
      }
      else {
        showMessage("hour",`${counterHour}`);
      }
      counterSecond++;
    };

// Функція запускає таймер та керує кольором фону таймера
get("startBtn").onclick = () => {
  if(!flag){
    options();

    intervalHandler = setInterval(count, setInt);  
    flag = true;
    classRemAdd("stopwatch","green",'container-stopwatch');
  };
};

 // Функція зупиняє таймер та керує кольором фону таймера
get("stopBtn").onclick = () => {
  if(flag){
    clearInterval(intervalHandler);
    classRemAdd("stopwatch","red",'container-stopwatch');
    showMessage('mark1',':');
    showMessage('mark2',':');
    flag = false;
  };
};

 // Функція обнуляє таймер та керує кольором фону таймера
get("resetBtn").onclick = () => {
  if(!flag){
   // Оновлення значеннь на дісплеї
    showMessage("second",`00`);
    showMessage("minut",`00`);
    showMessage("hour",`00`);
   // Оновлення значеннь лічильників
    counterHour = 0;
    counterMinut  = 0;
    counterSecond = 0;

    classRemAdd("stopwatch","silver",'container-stopwatch');
  };
};

