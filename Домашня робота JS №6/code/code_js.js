'use strict'// Суворий режим

// Оголошення функцій

// Функція для випадаючого меню 
function showMenu() {
    document.getElementById("myDropdown").classList.toggle("show");
};
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    let dropdowns = document.getElementsByClassName("dropdown-content");
    for (let i = 0; i < dropdowns.length; i++) {
      let openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      };
    };
  };
};

// Функція виводу данних на сторінку selector = сclass="Куди потрібно вивести",text = Текст який потрібно вивести.position - "afterBegin" перед попереднім   "beforeEnd"  після попереднього
function showResult(selector,text = `<p>Результат виконання: <span class="red">Не виконано</span></p>`,position = "beforeEnd"){
    document.querySelector(selector).insertAdjacentHTML(`${position}`,
    text);
};

// Домашня робота №6

/*Завдання №1
Реалізуйте клас Worker (Працівник), 
який матиме такі властивості: 
name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.*/

// Створення класу Worker
class Worker {
  constructor (name,surname,rate,days){
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
  };
};
// Створення прототипу для класу Worker
Worker.prototype.getSalary = function(){
  return this.rate * this.days;
};
// Створення екзимплярів класу Worker
const worker1 = new Worker("Влад","Бродич",200,30);
const worker2 = new Worker("Тетяна","Хомка",250,30);
const worker3 = new Worker("Олександр","Арестович",150,30);
const worker4 = new Worker("","",120,0);
const worker5 = new Worker("","",100,0);

// Функція перевіряє яку позицію обрав користувач зі списку
function validEmployees(){
  let el_pr = document.getElementById('el_pr');
  let el = el_pr.value;
  if(el === "worker1"){
    worker1.job = 'Front-End Розробник';
    return worker1;
  }
  else if(el === "worker2"){
    worker2.job = 'Project-Менеджер';
    return worker2;
  }
  else if(el === "worker3"){
    worker3.job = 'Бізнес-Аналітик';
    return worker3;
  }
  else if(el === "worker4"){
    worker4.job = 'Тестувальник';
    return worker4;
  }
  else if(el === "worker5"){
    worker5.job = 'UI/UX Дізайнер';
    return worker5;
  }
  else alert('Оберіть спеціаліста');
};

// Функція внесення змін до картки працівника
function recordEmployees(){
  validEmployees().name = prompt("Напишіть І'мя працівника", validEmployees().name);
  validEmployees().surname = prompt("Напишіть Призвище працівника", validEmployees().surname);
  validEmployees().rate =  parseInt(prompt("Тариф за день", validEmployees().rate));
};

// Функція вивід інформації на сторінку
function showEmployees(){
  if(validEmployees().name === ''&& validEmployees().surname === '' ){
    showResult('.js-box-1', `<p>Посада  ${validEmployees().job}: <span class="red">Вакансія вільна</span><br> Заробітня плата: <span class="red">${validEmployees().rate} $</span> на день</p>`,"afterBegin");
  }
  else{
    showResult('.js-box-1', `<p>Посада ${validEmployees().job}:<br> І'мя: ${validEmployees().name}<br> Призвище: ${validEmployees().surname}<br> Заробітня плата за ${validEmployees().days = parseFloat(prompt("Кількість робочих днів день", "0"))} днів складає: <span class="red">${validEmployees().getSalary()} $ </span></p>`,"afterBegin");
  }
};

//Завдання №2
/*Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
який параметром приймає рядок, а повертає її в перевернутому вигляді,
метод ucFirst(),
який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.*/

// Створення класу MyString
class MyString {
  constructor(str){
    this.str = str;
  };
};

// Створення прототипів класу MyString
// метод reverse()
MyString.prototype.reverse = function (){
  if(this.str === ''){
    return 'Рядок порожній!';
  }
  else{
    let arrStr = this.str.split(""); //Перетворює строку на тимчасовий масив
    arrStr = arrStr.reverse(); //Змінює порядок індексів в масиві
    this.str = arrStr.join(""); //Перетворює тимчасовий масив на строку
    return this.str;

  // return this.str.split("").reverse().join(""); //Той самий код. Короткий запис
  };
};

// метод ucFirst()
MyString.prototype.ucFirst = function (){
  if(this.str === ''){
    return 'Рядок порожній!';
  }
  else{
    let arrStr = this.str.trim(""); //Прибирає пробіли з початку та в кінці строки
    arrStr = arrStr.split(""); //Перетворює строку на тимчасовий масив
    arrStr[0] = arrStr[0].toUpperCase(); //Переводить літеру з індексом 0 в вехній регістр
    this.str = arrStr.join(""); //Перетворює тимчасовий масив на строку
    return this.str;
  };
};

// метод ucWords()
MyString.prototype.ucWords = function(){
  if(this.str === ''){
    return 'Рядок порожній!';
  }
  else{
  let arrStr = this.str.trim(""); //Прибирає пробіли з початку та в кінці строки
  arrStr = arrStr.split(""); //Перетворює строку на тимчасовий масив
  arrStr[0] = arrStr[0].toUpperCase();
  for(let i = 0; i < arrStr.length; i+=1){
    if(arrStr[i] === ' '){
      arrStr[i+1] = arrStr[i+1].toUpperCase();//Переводить наступну за пробілом літеру  в вехній регістр
    };
  };
  this.str = arrStr.join(""); //Перетворює тимчасовий масив на строку
  return this.str ;
  };
};
// Створення екземплярів класу MyString
const rst1 = new MyString('метод reverse(), який параметром приймає рядок, а повертає її в перевернутому вигляді');
const rst2 = new MyString('метод ucFirst(), який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою');
const rst3 = new MyString('метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.');
const rst4 = new MyString('');


// Функція вивід на сторінку результату виконання методу reverse() 
function showReverse(){
  rst1.str = prompt("Введіть тект", "метод reverse(), який параметром приймає рядок, а повертає її в перевернутому вигляді");
  showResult('.js-box-2', `<p>Результат виконання:<br>До: <span class="red">"${rst1.str}"</span><br>Після: <span class="green">"${rst1.reverse()}"</span></p>`,"afterBegin");
};

// Функція вивід на сторінку результату виконання методу ucFirst()
function showUcFirst(){
  rst2.str = prompt("Введіть тект", "метод ucFirst(), який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою");
  showResult('.js-box-3', `<p>Результат виконання:<br>До: <span class="red">"${rst2.str}"</span><br>Після: <span class="green">"${rst2.ucFirst()}"</span></p>`,"afterBegin");
};

// Функція вивід на сторінку результату виконання методу ucWords()
function showUcWords(){
  rst3.str = prompt("Введіть тект", "метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.");
  showResult('.js-box-4', `<p>Результат виконання:<br>До: <span class="red">"${rst3.str}"</span><br>Після: <span class="green">"${rst3.ucWords()}"</span></p>`,"afterBegin");
};

//Завдання №3
/*Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". 
Метод getNumber повертає номер телефону.
Викликати ці методи кожного з об'єктів.*/

// Створення класу Phone
class Phone {
  constructor (number, model, weight) {
    this.number = number;
    this.model = model;
    this.weight = weight;
  };
};

// Створення прототипів класу Phone

// метод receiveCall()
Phone.prototype.receiveCall = function(name){
  this.name = name;
  return `Телефонує ${this.name}`;
};
// метод getNumber()
Phone.prototype.getNumber = function(){
  return this.number;
};
// Створення екземплярів класу Phone
const phone1 = new Phone ('068-333-00-22', 'Apple iPhone 14 Pro', 206);
const phone2 = new Phone ('067-333-22-22', 'Samsung Galaxy A04', 192);
const phone3 = new Phone ('068-222-00-00', 'Xiaomi 12S', 179);

// Функція перевіряє яку позицію обрав користувач зі списку
function validPhones(phoneImg){
  let el__list = document.getElementById('el__list');
  let el = el__list.value;
  if(el === "phone1"){
    phone1.img = '<img src="../imege/apple.jpg" alt="apple" width="100">';
    return phone1;
  }
  else if(el === "phone2"){
    phone2.img  = '<img src="../imege/samsung-galaxy.jpg" alt="samsung" width="100">';
    return phone2;
  }
  else if(el === "phone3"){
    phone3.img  = '<img src="../imege/xiaomi-12S.jpg" alt="xiaomi" width="100">';
    return phone3;
  }
  else alert('Оберіть модель');
};

// Функція вивід на сторінку результату виконання методу receiveCall()
function showReceiveCall(){
  showResult('.js-box-5__el', `Результат виклику "receiveCall()": <span class="green">${validPhones().receiveCall(prompt("Введіть ім'я", "ім'я"))}</span><br>`);
};

// Функція вивід на сторінку результату виконання методу getNumber()
function showGetNumber(){
  showResult('.js-box-5__el', `Результат виклику "getNumber()": <span class="green">Номер: ${validPhones().getNumber()}</span><br>`);
};

// Функція вивід на сторінку значення змінних екземплярів класу Phone
function showPhone(){
  showResult('.js-box-5', `<p  class="js-box-5__el">${validPhones().img}<br>Модель: <span class="red">${validPhones().model}</span><br>Номер: <span class="green">${validPhones().number}</span><br>Вага: <span class="green">${validPhones().weight} г.</span><br><button onclick="showReceiveCall()" class="work-3__btn1" type="button">Викликати метод "receiveCall()" ?</button><br><button onclick="showGetNumber()" class="work-3__btn1" type="button">Викликати метод "getNumber()" ?</button><br></p>`,"afterBegin");
};

//Завдання №4
/*Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.*/
// https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png

// Створення класу Водій
class Driver{
  constructor (fulname,experinece){
    this.fulname = `Водій: ${fulname}`;
    this.experinece = `Водійський стаж: ${experinece} р.`;
  };
};

// Створення класу Двигун
class Engine{
  constructor (powerEngine,producerEngine){
    this.powerEngine = `Потужність двигуна: ${powerEngine} К/с`;
    this.producerEngine = `Виробник: ${producerEngine}`;
  };
};

// Створення класу Автомобіль
class Car{
  constructor (brandCar,classCar,weightCar,driver,engine){
    this.brandCar = `Марка авто: "${brandCar}"`;
    this.classCar = `Клас авто: ${classCar}`;
    this.weightCar = `Вага авто: ${weightCar} т.`;
    this.driver = driver;
    this.engine = engine;
  };
};
// Створення прототипу для класу Car
Car.prototype.start = function(){
  return "Поїхали";
};
Car.prototype.stop = function(){
  return "Зупиняємося";
};
Car.prototype.turnRight = function(){
  return "Поворот праворуч";
};
Car.prototype.turnLeft = function(){
  return "Поворот ліворуч";
};
Car.prototype.toString = function(){
  let result = '';
  for(let i in this){
    if(this.hasOwnProperty(i)){
      if(typeof this[i] === 'object'){
        for(let a in this[i]){
          if(this[i].hasOwnProperty(a)){
            result += `${this[i][a]} <br>`;
          }; 
        };
      }
      else{
        result += `${this[i]} <br>`;
      };
    };
  };
  console.log(result);
  return result;
};

// Створення похідного класу Вантажівка
class Lorry extends Car{
  load =  "Корисне навантаження: 20 т.";
};

// Створення похідного класу Спортивний автомобіль
class SportCar extends Car{
  maxSpeed =  "Максимальна швидкість: 280 Км/год.";
};

// Створення екземплярів класу Driver
const driver = new Driver("Влад Бродич",20);
const driverLorry = new Driver("Влад Бродич",30);
const driverSportCar = new Driver("Влад Бродич",25);

// Створення екземплярів класу Engine
const engine = new Engine (240,"Німеччина");
const engineLorry = new Engine (500,"Шведція");
const engineSportCar = new Engine (150,"Японія");

// Створення екземплярів класу Car,Lorry,SportCar
const car = new Car("Volkswagen Touareg","Позашляховик",2,driver,engine);
const carLorry = new Lorry("Volvo FH","Вантажівка",7,driverLorry,engineLorry);
const sportCar = new SportCar("Subaru Impreza","Спорт-Кар",1.5,driverSportCar,engineSportCar);

// Функція перевіряє яку позицію обрав користувач зі списку
function validCar(){
  let el_pr = document.getElementById('el__list-4');
  let el = el_pr.value;
  let carModel = '';
  if(el === "car1"){
    return carModel = car;
  }
  else if(el === "car2"){
    return carModel = carLorry;
  }
  else if(el === "car3"){
    return carModel = sportCar;
  }
  else alert('Оберіть авто');
};

// Функція вивід на сторінку результату виконання методу .start()
function showStart(){
  showResult('.js-box-6__el', `Результат виконання ".start()": <span class="green">"${validCar().start()}"</span><br>`);
};
// Функція вивід на сторінку результату виконання методу .stop()
function showStop(){
  showResult('.js-box-6__el', `Результат виконання ".stop()": <span class="red">"${validCar().stop()}"</span><br>`);
};
// Функція вивід на сторінку результату виконання методу .turnRight()
function showTurnRight(){
  showResult('.js-box-6__el', `Результат виконання ".turnRight()": <span class="red">"${validCar().turnRight()}"</span><br>`);
};
// Функція вивід на сторінку результату виконання методу .turnLeft()
function showTurnLeft(){
  showResult('.js-box-6__el', `Результат виконання ".turnLeft()": <span class="green">"${validCar().turnLeft()}"</span><br>`);
};
// Функція вивід на сторінку результату виконання методу .toString()
function showCar(){
  showResult('.js-box-6', `<p class="js-box-6__el">Результат виконання:<br><span class="green">${validCar().toString()}<br></span><br><button onclick="showStart()" class="work-3__btn1" type="button">Викликати метод "start()" ?</button><button onclick="showStop()" class="work-3__btn1" type="button">Викликати метод ".stop()" ?</button><br><button onclick="showTurnLeft()" class="work-3__btn1" type="button">Викликати метод ".turnLeft()" ?</button><button onclick="showTurnRight()" class="work-3__btn1" type="button">Викликати метод "turnRight()" ?</button><br></p>`,"afterBegin");
};


//Завдання №4
/*Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.

Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.*/

// Створення прототипу для класу Animal
class Animal {
  constructor(name) {
    this.name = name;
    this.food = 'Їжа';
    this.location = 'Дім';
    animal.push(this);
  };
};
const animal = [];
// Створення прототипу для класу Animal
Animal.prototype.makeNoise = function(){
  return `Така тварина гуляє біля ${this.location}`;
};
Animal.prototype.eat = function (){
  return `${this.name} їсть ${this.food}`;
};
Animal.prototype.sleep = function(){
  return `"${this.name}" спить`;
};

// Створення похідного класу Dog
class Dog extends Animal {
    food = 'собачий корм';
    location = "будинок";
    species = 'Джек-рассел-терьер';
};
Dog.prototype.makeNoise = function(){
  return `${this.species} пес-"${this.name}" охороняє ${this.location}`;
};
Dog.prototype.eat = function (){
  return `Пес-"${this.name}" їсть ${this.food}`;
};

// Створення похідного класу Cat
class Cat extends Animal {
  food = 'котячий корм';
  location = 'квартира';
  characteristic = 'Пухнастий';
}
Cat.prototype.makeNoise = function(){
  return `${this.characteristic} кіт "${this.name}" ловить мишей`;
};
Cat.prototype.eat = function (){
  return `Кіт "${this.name}" їсть ${this.food}`;
};

// Створення похідного класу Horse
class Horse extends Animal {
  food = 'сіно';
  location = 'стайня';
  color = 'Рудий';
};
Horse.prototype.makeNoise = function(){
  return `${this.color} кінь "${this.name}" катає дітей`;
};
Horse.prototype.eat = function (){
  return `${this.color} кінь "${this.name}" їсть ${this.food}`;
};

// Створення екземплярів класів
let dog1 = new Dog("Патрон");
let cat1 = new Cat("Мурчик");
let horse1 = new Horse("Яблучко");


// Створення прототипу для класу Veterinar
class Veterinary {
};
Veterinary.prototype.void = function (){
  let obj = veterinary.main();
  let result = '';
  if(typeof obj === 'object'){
    result += `Ім'я тварини: ${obj.name}<br>`;
    result += `Що їсть: ${obj.food}<br>`;
    result += `Де мешкає: ${obj.location}<br>`;
    return result;
  }
  else{
    return obj;
  };
};
Veterinary.prototype.main = function(){
  main.i += 1;
  if(animal.length >= main.i){
    return animal[main.i - 1];
  }
  return 'Запис закінчено';
};
main.i = 0;
// Створення екземплярів класу Veterinary
const veterinary = new Veterinary();

// Функція перевіряє яку позицію обрав користувач зі списку
function validAnimal(){
  let el_pr = document.getElementById('el__list-5');
  let el = el_pr.value;
  let animalName = '';
  if(el === "animal1"){
    return animalName = dog1;
  }
  else if(el === "animal2"){
    return animalName = cat1;
  }
  else if(el === "animal3"){
    return animalName = horse1;
  }
  else alert('Оберіть тварину');
};

// Функція вивід на сторінку результату виконання методу .makeNoise()
function showMakeNoise(){
  showResult('.js-box-7__el', `Результат виконання ".makeNoise()": <span class="green">${validAnimal().makeNoise()}</span><br>`);
};
// Функція вивід на сторінку результату виконання методу .eat()
function showEat(){
  showResult('.js-box-7__el', `Результат виконання ".eat()": <span class="green">${validAnimal().eat()}</span><br>`);
};
// Функція вивід на сторінку результату виконання методу .sleep()
function showSleep(){
  showResult('.js-box-7__el', `Результат виконання ".sleep()": <span class="green">${validAnimal().sleep()}</span><br>`);
};
// Функція вивід на сторінку результату виконання методу .toString()
function showAnimaly(){
  showResult('.js-box-7', `<p class="js-box-7__el">Оберіть що  робить тваринка:<br><span class="green"></span><br><button onclick="showMakeNoise()" class="work-3__btn1" type="button">Викликати метод "makeNoise" ?</button><button onclick="showEat()" class="work-3__btn1" type="button">Викликати метод ".eat()" ?</button>  <button onclick="showSleep()" class="work-3__btn1" type="button">Викликати метод ".sleep()" ?</button><br></p>`,"afterBegin");
};
// Функція вивід на сторінку результату виконання методу Ветеринар
function showVeterinary(){
  showResult('.js-box-8', `<p class="js-box-6__el">Інформація про тваринку:<br><span class="green">${veterinary.void()}<br></span></p>`,"afterBegin");
};